-- ohrm_module
INSERT INTO `ohrm_module` (`id`, `name`, `status` ) VALUES ( '14', 'resourceReport', '1' ) ;

-- ohrm_screen :
INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES ('116', 'Resource Report', '14', 'ResourceReport');
INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES ('117', 'Engineer Resource Utilization Detail', '14', 'EngineerResourceUtilization_TCI');
INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES ('118', 'Project Resource Utilization', '14', 'ProjectResourceUtilization_TCI');
INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES ('119', 'Timesheet Details', '14', 'ViewTemplate');
INSERT INTO `ohrm_screen` (`id`, `name`, `module_id`, `action_url`) VALUES ('120', 'Leave Report', '14', 'LeaveReport');


-- ohrm_menu_item :
INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES ('93', 'Resource Report', '116', NULL, '1', '800', NULL, '1');
INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES ('94', 'Engineer Resource Utilization Detail', '117', '93', '2', '100', NULL, '1');
INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES ('95', 'Project Resource Utilization', '118', '93', '2', '200', NULL, '1');
INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES ('96', 'Timesheet Details', '119', '93', '2', '300', NULL, '1');
INSERT INTO `ohrm_menu_item` (`id`, `menu_title`, `screen_id`, `parent_id`, `level`, `order_hint`, `url_extras`, `status`) VALUES ('97', 'Leave Report', '120', '93', '2', '400', NULL, '1');


-- ohrm_user_role_screen  :
INSERT INTO `ohrm_user_role_screen` ( `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES ( '1', '116', '1', '1', '1', '1');
INSERT INTO `ohrm_user_role_screen` ( `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES ( '1', '117', '1', '1', '1', '1');
INSERT INTO `ohrm_user_role_screen` ( `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES ( '1', '118', '1', '1', '1', '1');
INSERT INTO `ohrm_user_role_screen` ( `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES ( '1', '119', '1', '1', '1', '1');
INSERT INTO `ohrm_user_role_screen` ( `user_role_id`, `screen_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES ( '1', '120', '1', '1', '1', '1');