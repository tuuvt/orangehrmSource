 /**
 * 
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_Success.js 
 * 2015-07-22
 * @description :
 * validate user's input
 */
$(document).ready(function() {
    $('#resourceReport_employeeName_empName').click(function(){
        if($(this).val() === "Type for hints...")
            $(this).val('');
    })
    $('#btnSrch').click(function() {
        $('#frmSrchLeaveReport').submit();
   
    });
    $('#btnRst').click(function() {
        location.replace(location);
    });
    
    $('#report').click(function() {
        $('#btnSrch').load(myReload());
        $('#exportExcel').val(1);
        $('#frmSrchLeaveReport').submit();
        
        $('#exportExcel').val('');
    });
	
	$('#resourceReport_employeeName_empName').click(function(){
		if($(this).val() === 'Type for hints...')
			$(this).val('');
	});
    
    $('#resourceReport_employeeName_empName').result(function(event, item) {
        $(this).valid();
    });
    
    
    var validator = $("#frmSrchLeaveReport").validate({

        rules: {
            'resourceReport[reportFromDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                }
            },
            'resourceReport[reportToDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                },
                date_range: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        fromDate:$('#report_fromDate').val()
                    }
                }
            },
            'resourceReport[employeeName][empName]':{
        		empNameValidation: true,
            }
        },
        messages: {
            'resourceReport[reportFromDate]' : {
                valid_date: lang_validDateMsg
            },
            'resourceReport[reportToDate]' : {               
                valid_date: lang_validDateMsg,
                date_range: lang_dateError                        
            },
            'resourceReport[employeeName][empName]':{
                empNameValidation: lang_nameError,
            }
        },          
    });
    
    $.validator.addMethod("empNameValidation", function(value, element, params) {
        var valid = false;
        var empCount = employeeArray.length;
        if ($('#resourceReport_employeeName_empName').val() === "Type for hints...") {
        	$('#resourceReport_employeeName_empId').val("");
        	valid = true;
        }

        else if ($('#resourceReport_employeeName_empName').val() == "") {
        	$('#resourceReport_employeeName_empId').val("");
        	valid = true;
        }
        else{
            var i;
            for (i=0; i < empCount; i++) {
                empName = $.trim($('#resourceReport_employeeName_empName').val());
                arrayName = employeeArray[i].name;
                if (empName == arrayName) {
                	valid = true
                    break;
                }
            }
        }
        return valid;
    });
    
    var Type = 'odd';
     $('#resultTable tbody tr').each(function() {
        $(this).removeClass().addClass(Type);
        $(this).find('td').each(function() {
            if ( ($(this).text() == "Total") ) {
                $(this).parent().css("border-bottom","1px solid #989898");
                (Type=="odd")? Type="even" : Type="odd";
                return Type;
            }
        });
    });
    
    $('#frmList_ohrmListComponent').attr('name','frmList_ohrmListComponent');

});
