<?php
/**
 * 
 * @author Nguyen Thanh Dat
 * @name: ResourceReportDao
 * 2015-07-22
 * @lastModified by: Luu Quoc Bao - 12/08/2015
 * @reason: Refactory code and update comment
 * @description :
 * Class hold Function to call query to database
 * function getNAME_OF_REPORT_Data($srchParams) : call this function to get data of report
 * function getNAME_OF_REPORT_COUNT($srchParams): call this function to count number of record use for paging
 * function get_NAME_OF_PREOJECT_Query()        : call this function to get SQL_QUERY for the project.
 * function get_PROJECTLIST()                   : call this function to get list of project (use for select dropdown)
 * 
 */
class ResourceReportDao {
	
	/**
	 * get data of report Engineer Resource Utilization Detail
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$srchParams
	 * @return $result
	 */
	public function getEngineerResourceUtilizationDetail_Data(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_ERUD_Query ();
			
			$limit = $srchParams->getLimit ();
			if ($limit) {
				$q .= " LIMIT " . $srchParams->getOffset () . ", " . $limit;
			}
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$eId = $srchParams->getEmployeeNumber ();
			$pName = $srchParams->getProjectName ();
            $totalTime = $srchParams->getTotalTime();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"eId" => $eId,
					"pName" => $pName,
                    "totalTime" => $totalTime 
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			$results ['data'] = $data;
			$results ['count'] = $this->getEngineerResourceUtilizationDetail_Data_Count ( $srchParams );
			return $results;
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 * Count the object of the EngineerResourceUtilizationDetail_Data
	 * 24/07/2015
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$srchParams
	 * @return $result
	 */
	public function getEngineerResourceUtilizationDetail_Data_Count(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_ERUD_Query ();
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$eId = $srchParams->getEmployeeNumber ();
			$pName = $srchParams->getProjectName ();
            $totalTime = $srchParams->getTotalTime();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"eId" => $eId,
					"pName" => $pName,
                    "totalTime" => $totalTime                     
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			return count ( $data );
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 * get data of report Engineer Resource Utilization Detail
	 *
	 * @author Le Minh Duc
	 * @param
	 *        	$srchParams
	 * @return $result
	 */
	public function getProjectResourceUtilization_Data(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_PRU_Query ();			
			$limit = $srchParams->getLimit ();
			if ($limit) {
				$q .= " LIMIT " . $srchParams->getOffset () . ", " . $limit;
			}
			
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$pName = $srchParams->getProjectName ();
            $totalTime = $srchParams->getTotalTime();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"pName" => $pName,
                    "totalTime" => $totalTime 
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			$results ['data'] = $data;
			$results ['count'] = $this->getProjectResourceUtilization_Data_Count ( $srchParams );
			
			return $results;
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 * Count the object of the ProjectResourceUtilization_Data
	 * 24/07/2015
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$srchParams
	 * @return $result
	 */
	public function getProjectResourceUtilization_Data_Count(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_PRU_Query ();
			
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$pName = $srchParams->getProjectName ();
            $totalTime = $srchParams->getTotalTime();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"pName" => $pName,
                    "totalTime" => $totalTime 
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			
			return count ( $data );
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 *
	 * @author Nguyen Nhat Han
	 * @name : get data for report: TimeSheetDetail
	 *       2015-07-22
	 *       get data for TimeSheetDetail
	 *      
	 */
	public function getTimeSheet_Data(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_TSD_Query ();
			
			$limit = $srchParams->getLimit ();
			if ($limit) {
				$q .= " LIMIT " . $srchParams->getOffset () . ", " . $limit;
			}
			
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$params = array (
					"from_date" => $srchParams->getFromDate (),
					"to_date" => $srchParams->getToDate (),
					"eID" => $srchParams->getEmployeeNumber (),
					"pName" => $srchParams->getProjectName () 
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			$results ['data'] = $data;
			$results ['count'] = $this->getTimeSheet_Data_Count ( $srchParams );
			return $results;
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 *
	 * @author Vo Tien Thieu
	 * @param ResourceReportSearchParameters $srchParams        	
	 * @throws DaoException
	 * @return multitype:
     * @description Count the object of the TimeSheet_Data
	 */
	public function getTimeSheet_Data_Count(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_TSD_Query ();
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$params = array (
					"from_date" => $srchParams->getFromDate (),
					"to_date" => $srchParams->getToDate (),
					"eID" => $srchParams->getEmployeeNumber (),
					"pName" => $srchParams->getProjectName () 
			);
			
			$stmt->execute ( $params );
			$results = $stmt->fetchAll ();
			return count ( $results );
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	
	/**
	 *
	 * @author Luu Quoc Bao
	 * @name : getProjectNameList
	 *       2015-07-29
	 *       get list: name of project
	 */
	public function getProjectNameList($excludeDeletedProjects = true, $orderField = 'project_id', $orderBy = 'ASC') {
		try {
			
			$q = "SELECT p.project_id AS projectId, p.name AS projectName, c.name AS customerName
            		FROM ohrm_project p
            		LEFT JOIN ohrm_customer c ON p.customer_id = c.customer_id";
			
			if ($excludeDeletedProjects) {
				$q .= " WHERE p.is_deleted = 0";
			}
			
			if ($orderField) {
				$orderBy = (strcasecmp ( $orderBy, 'DESC' ) == 0) ? 'DESC' : 'ASC';
				$q .= " ORDER BY {$orderField} {$orderBy}";
			}
			
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$projectList = $pdo->query ( $q )->fetchAll ( PDO::FETCH_ASSOC );
			
			return $projectList;
			
			// @codeCoverageIgnoreStart
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage (), $e->getCode (), $e );
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * @author Vo Tien Thieu
	 * @param ResourceReportSearchParameters $srchParams
	 * @throws DaoException
	 * @return result
     * @description get data for report : Leave-report
	 */
	public function getLeaveReport_Data(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_LR_Query ();
			$limit = $srchParams->getLimit ();
			if ($limit) {
				$q .= " LIMIT " . $srchParams->getOffset () . ", " . $limit;
			}
			
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$eId = $srchParams->getEmployeeNumber ();
			$type = $srchParams->getLeaveType ();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"eID" => $eId,
					"type" => $type 
			);
			$stmt->execute ( $params );
			$data = $stmt->fetchAll ();
			$results ['data'] = $data;
			
			$results ['count'] = $this->getLeaveReport_Data_count ( $srchParams );
			return $results;
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	/**
	 * @author Vo Tien Thieu
	 * @param ResourceReportSearchParameters $srchParams
	 * @throws DaoException
	 * @return number
	 */
	public function getLeaveReport_Data_count(ResourceReportSearchParameters $srchParams) {
		try {
			$q = $this->get_LR_Query ();
			$pdo = Doctrine_Manager::connection ()->getDbh ();
			$stmt = $pdo->prepare ( $q );
			$from_date = $srchParams->getFromDate ();
			$to_date = $srchParams->getToDate ();
			$eId = $srchParams->getEmployeeNumber ();
			$type = $srchParams->getLeaveType ();
			$params = array (
					"from_date" => $from_date,
					"to_date" => $to_date,
					"eID" => $eId,
					"type" => $type 
			);
			$stmt->execute ( $params );
			$results = $stmt->fetchAll ();
			return count ( $results );
		} catch ( Exception $e ) {
			throw new DaoException ( $e->getMessage () );
		}
	}
	/**
	 *
	 * @author Le Minh Duc
	 * @return $q - Query for Engineer Resource Utilization Detail
	 *         @date 28/07/2015
	 */
	private function get_ERUD_Query() {
		$q = "
                SELECT
                    useForSort.eID as eID,
                	useForSort.eName as eNameUseForSort,
                	useForSort.eName as EmployeeName,
                	useForSort.type as type,
                	'' as Target,
                	useForSort.pRole as Project_role,
                	useForSort.percent as ActualPercent,	
                	CASE SUBSTRING(useForSort.Hours, -1, 1)
                		WHEN 0 THEN SUBSTRING(useForSort.Hours, 1, LENGTH(useForSort.Hours) - 2)
                		ELSE useForSort.Hours
                	END as Hours,
                	useForSort.pName as Project,
                	useForSort.note as Note,
                	useForSort.sort as sort
                FROM
                	(
                        SELECT
                            fullDataAfterUnion.eName as eName,  
                            fullDataAfterUnion.eID as eID,
                            fullDataAfterUnion.type as type,
                        	fullDataAfterUnion.pRole as pRole,
                            CONCAT(ROUND(fullDataAfterUnion.Hours/ :totalTime * 100, 2), '%') as percent,
                            ROUND(fullDataAfterUnion.Hours, 1) as Hours,
                            fullDataAfterUnion.pName as pName,
                			fullDataAfterUnion.note as note,
                            fullDataAfterUnion.sort as sort
                        FROM
                            (
                                SELECT
                                    t.employee_id as eID,
                                    CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as eName,
                                    IF(p.name REGEXP '_B_', 'Bill', 'Non Bill') as type,
                					e.custom1 as pRole,
                                    SUM(t.duration)/3600 as Hours,
                                    p.name as pName,
                					p.description as note,
                                    IF(p.name REGEXP '_B_', 'a', 'b') as sort
                                FROM
                                    ohrm_timesheet_item as t
                                    LEFT JOIN
                                        ohrm_project as p
                                    ON
                                        t.project_id = p.project_id
                                    LEFT JOIN
                                        hs_hr_employee as e
                                    ON
                                        t.employee_id = e.emp_number
                                WHERE
                                    e.custom1 IS NOT NULL
									AND t.duration <> 0
                					AND t.date BETWEEN :from_date AND :to_date
                                    AND (p.name REGEXP '_B_' OR p.name REGEXP '_N_')
                                GROUP BY
                                    eID, t.project_id
                
                                UNION
                
                                SELECT
                					allEmpWorkAtThisTime.eID as eID,
                					allEmpWorkAtThisTime.eName as eName,
                					'NonBill' as type,
                					allEmpWorkAtThisTime.pRole as pRole,
                					IF(leaveEmp.Hours IS NULL, 0, leaveEmp.Hours) as Hours,
                					'Leave' as pName,
                					allEmpWorkAtThisTime.note as note,
                					'c' as sort
                				FROM
                					(
                						SELECT
                							t.employee_id as eID,
                							CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as eName,
                							e.custom1 as pRole,
                							p.description as note
                						FROM
                							ohrm_timesheet_item as t
                							LEFT JOIN
                								hs_hr_employee as e
                							ON
                								e.emp_number = t.employee_id
                							LEFT JOIN
                								ohrm_project as p
                							ON
                								p.project_id = t.project_id
                						WHERE
                							t.date BETWEEN :from_date AND :to_date
                							AND e.custom1 IS NOT NULL
                						GROUP BY
                							eID    
                					) as allEmpWorkAtThisTime
                
                					LEFT JOIN
                					(
                						SELECT
                							l.emp_number as eID,
                							SUM(IF(l.length_hours IS NULL, 0, l.length_hours)) as Hours
                						FROM
                							ohrm_leave as l
                							LEFT JOIN
                								hs_hr_employee as e
                							ON
                								l.emp_number = e.emp_number
                						WHERE
                							l.status > 0
                							AND e.custom1 IS NOT NULL
                							AND l.date BETWEEN :from_date AND :to_date	
                						GROUP BY
                							eID
                					) as leaveEmp
                					ON
                						leaveEmp.eID = allEmpWorkAtThisTime.eID
                						
                				UNION
                				
                				SELECT
                					leaveEmp.eID as eID,
                					leaveEmp.eName as eName,
                					'NonBill' as type,
                					leaveEmp.pRole as pRole,
                					IF(leaveEmp.Hours IS NULL, 0, leaveEmp.Hours) as Hours,
                					'Leave' as pName,
                					'' as note,
                					'c' as sort
                				FROM
                					(
                						SELECT
                							t.employee_id as eID
                						FROM
                							ohrm_timesheet_item as t
                							LEFT JOIN
                								hs_hr_employee as e
                							ON
                								e.emp_number = t.employee_id
                						WHERE
                							t.date BETWEEN :from_date AND :to_date
                							AND e.custom1 IS NOT NULL
                						GROUP BY
                							eID    
                					) as allEmpWorkAtThisTime
                
                					RIGHT JOIN
                					(
                						SELECT
                							l.emp_number as eID,
                							CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as eName,
                							e.custom1 as pRole,
                							SUM(IF(l.length_hours IS NULL, 0, l.length_hours)) as Hours
                						FROM
                							ohrm_leave as l
                							LEFT JOIN
                								hs_hr_employee as e
                							ON
                								l.emp_number = e.emp_number
                						WHERE
                							l.status > 0
                							AND e.custom1 IS NOT NULL
                							AND l.date BETWEEN :from_date AND :to_date
                						GROUP BY
                							eID
                					) as leaveEmp
                					ON
                						leaveEmp.eID = allEmpWorkAtThisTime.eID
                				WHERE
                					allEmpWorkAtThisTime.eID is NULL
                            ) as fullDataAfterUnion
                        ORDER BY
                            eID, sort
                    ) useForSort
    			WHERE
                	IF(:eId = '', 1 = 1, eID = :eId)
                	AND IF(:pName = '', 1 = 1, useForSort.pName = :pName AND eID IN (
                		SELECT
                			t.employee_id as eID
                		FROM
                			ohrm_timesheet_item as t
                			LEFT JOIN
                				ohrm_project as p
                			ON
                				t.project_id = p.project_id
                			LEFT JOIN
                				hs_hr_employee as e
                			ON
                				t.employee_id = e.emp_number
                		WHERE
                			e.custom1 IS NOT NULL
                			AND t.date BETWEEN :from_date AND :to_date
                			AND (p.name REGEXP '_B_' OR p.name REGEXP '_N_')
                			AND (p.name = :pName)
                		GROUP BY
                			eID, t.project_id	
                	))		
                ORDER BY
                	eNameUseForSort, eID, sort, Project
							";
		return $q;
	}
	
	/**
	 *
	 * @author Le Minh Duc
	 * @return $q - Query for Project Resource Utilization
	 *         @date 28/07/2015
	 */
	private function get_PRU_Query() {
		$q = "
			SELECT
                useForROUND.type as type,
            	useForROUND.pName as NameOfProject,
            	'' as RequestBy,
            	'' as Target,
            	CONCAT(ROUND(useForROUND.percent, 2), '%') as ActualPercent,
            	CASE SUBSTRING(useForROUND.Hours, -1, 1)
            		WHEN 0 THEN IF(SUBSTRING(ROUND(useForROUND.Hours, 1), -1, 1) = 0, SUBSTRING(useForROUND.Hours, 1, LENGTH(useForROUND.Hours) - 3), SUBSTRING(useForROUND.Hours, 1, LENGTH(useForROUND.Hours) - 1))
            		ELSE useForROUND.Hours
            	END as ActualHours,
            	useForROUND.empcount as ProjectHeadCount,
            	'' as UtilizationMargin,
            	'' as HeadCount
            FROM
            	(
                    SELECT
                        useForGroupProject.type as type,
                        useForGroupProject.pName as pName,
                        SUM(useForGroupProject.percent)/COUNT(DISTINCT useForGroupProject.eID) as percent,
                        ROUND(SUM(useForGroupProject.Hours)/COUNT(DISTINCT useForGroupProject.eID), 2) as Hours,
                        COUNT(DISTINCT useForGroupProject.eID) as empCount,
                        useForGroupProject.sort as sort
                    FROM
                        (
                            SELECT
                                fullDataAfterUnion.eID as eID,
                                fullDataAfterUnion.type as type,
                                fullDataAfterUnion.Hours/:totalTime * 100 as percent,
                                ROUND(fullDataAfterUnion.Hours, 1) as Hours,
                                fullDataAfterUnion.pName as pName,
                                fullDataAfterUnion.sort as sort
                            FROM
                                (
                                    SELECT
                                        t.employee_id as eID,
                                        IF(p.name REGEXP '_B_', 'Bill', 'Non Bill') as type,
                                        SUM(t.duration)/3600 as Hours,
                                        p.name as pName,
                                        IF(p.name REGEXP '_B_', 'a', 'b') as sort
                                    FROM
                                        ohrm_timesheet_item as t
                                        LEFT JOIN
                                            ohrm_project as p
                                        ON
                                            t.project_id = p.project_id
                                        LEFT JOIN
                                            hs_hr_employee as e
                                        ON
                                            t.employee_id = e.emp_number
                                    WHERE
                                        e.custom1 IS NOT NULL
										AND t.duration <> 0
            							AND t.date BETWEEN :from_date AND :to_date							
                                        AND (p.name REGEXP '_B_' OR p.name REGEXP '_N_')
                                    GROUP BY
                                        eID, t.project_id
            
                                    UNION
            
                                    SELECT
                                        allEmpWorkAtThisTime.eID as eID,
                                        'Non Bill' as type,
                                        IF(leaveEmp.Hours IS NULL, 0, leaveEmp.Hours) as Hours,
                                        'Leave' as pName,
                                        'c' as sort
                                    FROM
                                        (
                                            SELECT
                                                t.employee_id as eID,
                                                CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as eName,
                                                e.custom1 as pRole,
                                                p.description as note
                                            FROM
                                                ohrm_timesheet_item as t
                                                LEFT JOIN
                                                    hs_hr_employee as e
                                                ON
                                                    e.emp_number = t.employee_id
                                                LEFT JOIN
                                                    ohrm_project as p
                                                ON
                                                    p.project_id = t.project_id
                                            WHERE
            									t.date BETWEEN :from_date AND :to_date
                                                AND e.custom1 IS NOT NULL
                                            GROUP BY
                                                eID    
                                        ) as allEmpWorkAtThisTime
            
                                        LEFT JOIN
                                        (
                                            SELECT
                                                l.emp_number as eID,
                                                SUM(IF(l.length_hours IS NULL, 0, l.length_hours)) as Hours
                                            FROM
                                                ohrm_leave as l
                                                LEFT JOIN
                                                    hs_hr_employee as e
                                                ON
                                                    l.emp_number = e.emp_number
                                            WHERE
                                                l.status > 0
                                                AND e.custom1 IS NOT NULL
            									AND l.date BETWEEN :from_date AND :to_date
                                            GROUP BY
                                                eID
                                        ) as leaveEmp
                                        ON
                                            leaveEmp.eID = allEmpWorkAtThisTime.eID
                                ) as fullDataAfterUnion
                            ORDER BY
                                eID, sort
                        ) as useForGroupProject
                    GROUP BY
                        pName
                    ORDER BY
                        type, sort, pName
                ) as useForROUND
            WHERE
            	IF(:pName = '', 1 = 1, useForROUND.pName = :pName)

            ";
		return $q;
	}
	
	/**
	 *
	 * @author Le Minh Duc
	 * @return $q - Query for TimeSheetDetail Report
	 *         @date 28/07/2015
	 */
	private function get_TSD_Query() {
		$q = "
			SELECT
			    endTable.EmployeeName as EmployeeName,
			    endTable.CountForProject as CountForProject,
			    endTable.Project as Project,
			    endTable.Note as Note,
			    endTable.Hours as Hours,
			    endTable.ODC as ODC,
			    endTable.eID as eID
			FROM
			    (
			        SELECT
			            @n := @n + 1 as No,
			            viewTemplateFinal.eID as eID,
			            viewTemplateFinal.EmployeeName as EmployeeName,
			            viewTemplateFinal.CountForProject as CountForProject,
			            viewTemplateFinal.Project as Project,
			            viewTemplateFinal.Note as Note,
			
			
			            viewTemplateFinal.Hours as Hours,
			            viewTemplateFinal.ODC as ODC,
			            viewTemplateFinal.sort	
			        FROM
			            (
			                 SELECT
			                    viewTemplateUnionLeave.eID as eID,
			                    viewTemplateUnionLeave.EmployeeName as EmployeeName,
			                    viewTemplateUnionLeave.CountForProject as CountForProject,
			                    viewTemplateUnionLeave.Project as Project,
			                    viewTemplateUnionLeave.Note as Note,
			                    viewTemplateUnionLeave.Hours as Hours,
			                    viewTemplateUnionLeave.ODC as ODC,
			                    viewTemplateUnionLeave.sort
			                FROM
			                    (
			                        SELECT
			                            e.emp_number as eID,
			                            concat(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as EmployeeName,
			                            IF(justWork.CountForProject IS NULL, '', justWork.CountForProject) as CountForProject,
			                            IF(justWork.Project IS NULL, '', justWork.Project) as Project,
			                            IF(justWork.Note IS NULL, '', justWork.Note) as Note,
			                            ROUND(IF(justWork.Hours IS NULL, '', justWork.Hours), 1) as Hours,
			                            IF(justWork.ODC IS NULL, '', justWork.ODC) as ODC,
			                            'a' as sort
			                        FROM
			                            (
			                                SELECT
			                                    t.employee_id as eID,
			                                    CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as EmployeeName,
			                                    IF(s.name IS NULL, '', s.name) as CountForProject,
			                                    p.name as Project,
			                                    ac.name as Note,
			                                    SUM(t.duration)/3600 as Hours,
			                                    IF (SUBSTRING(p.name, 1, 3) = 'ODC', 'X', '') as ODC
			                                FROM
			                                    ohrm_timesheet_item as t
			                                    LEFT JOIN
			                                        ohrm_project_activity as ac
			                                    ON
			                                        ac.activity_id = t.activity_id
			                                    LEFT JOIN
			                                        ohrm_project as p
			                                    ON
			                                        p.project_id = t.project_id
			                                    LEFT JOIN
			                                        hs_hr_employee as e
			                                    ON
			                                        e.emp_number = t.employee_id
			                                    LEFT JOIN
			                                        ohrm_subunit as s
			                                    ON
			                                        e.work_station = s.id
			                                    WHERE
			                                        e.custom1 IS NOT NULL
													AND t.duration <> 0
			                                        AND t.date BETWEEN :from_date AND :to_date
			                                GROUP BY
			                                t.employee_id, t.Project_id, t.activity_id
			                            ) as justWork
			
			                            RIGHT JOIN
			                                hs_hr_employee as e
			                            ON
			                                justWork.eID = e.emp_number
			                        WHERE
			                            e.custom1 IS NOT NULL
			
			                        UNION
			
			                        SELECT
			                            e.emp_number as eID,
			                            CONCAT(e.emp_firstname , ' ', e.emp_middle_name, ' ', e.emp_lastname) as EmployeeName,
			                            '' CountForProject,
			                            'Annual Leave' as Project,
			                            '' as Note,
			
			                            ROUND(IF(SUM(l.length_days)*8 IS NULL, 0, SUM(l.length_days)*8), 1) as Hour,
			                            '' as ODC,
			                            'b' as sort
			                        FROM
			                            ohrm_leave as l
			                            RIGHT JOIN
			                                hs_hr_employee as e
			                            ON
			                                l.emp_number = e.emp_number
			                                AND l.date BETWEEN :from_date AND :to_date
			                        WHERE
			                            e.custom1 IS NOT NULL
			                        GROUP BY
			                            eID    
			                    ) as viewTemplateUnionLeave,
			
			                    (
			                        SELECT @n := 0
			                    ) as stt
			                WHERE
			                	IF (:eID = '', 1 = 1, viewTemplateUnionLeave.eID = :eID)
			                    AND IF(:pName = '', 1= 1, (viewTemplateUnionLeave.Project = 'Annual Leave' OR viewTemplateUnionLeave.Project = :pName) AND viewTemplateUnionLeave.eID IN 
                                                                    (SELECT
                                                                            t.employee_id
                                                                    FROM
                                                                            ohrm_timesheet_item as t
                                                                            LEFT JOIN

                                                                                ohrm_project as p
                                                                            ON
                                                                                t.project_id = p.project_id
																			LEFT JOIN
																				hs_hr_employee as e
																			ON
																				e.emp_number = t.employee_id
                                                                    WHERE
                                                                            t.date BETWEEN :from_date AND :to_date
                                                                            AND p.name = :pName
																			AND e.custom1 IS NOT NULL
                                                                    GROUP BY
                                                                            t.employee_id, t.project_id))
			
			                ORDER BY
			                    eID, sort   
			            ) as viewTemplateFinal
			        GROUP BY
			            viewTemplateFinal.eID, No
			
			        ORDER BY
			            EmployeeName, No    
			    ) as endTable          
					            ";
		return $q;
	}
	
	/**
	 * @author Le Minh Duc
	 * @return string
	 * Query for Leave Report
	 */
	private function get_LR_Query() {
		$q = "
SELECT
					leaveTable.eName as eName,
					DATE_FORMAT(leaveTable.date, '%d/%m/%Y') as date,
					IF(leaveTable.date = '', 'Total', leaveTable.type) as type,
            		CASE SUBSTRING(leaveTable.number, -1, 1)
            			WHEN 0 THEN IF(SUBSTRING(ROUND(leaveTable.number, 1), -1, 1) = 0, SUBSTRING(leaveTable.number, 1, LENGTH(leaveTable.number) - 3), SUBSTRING(leaveTable.number, 1, LENGTH(leaveTable.number) - 1))
            			ELSE leaveTable.number
            		END as numDay,
					leaveTable.No as No,
					leaveTable.nameAo as nameAo
			FROM
			(
		        SELECT
		            @n := @n + 1 as No,
		            t1.eID as eID,
		            CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname) as nameAo,
		            t1.eName as eName,
		            IF(t1.date = 'a','', t1.date) as date,
		            IF(t1.date = '', 'total', t1.type) as type,
		            ROUND(t1.number, 2) as number
			        FROM
			            (
			                SELECT
			                    finalTable.eID as eID,
			                    IF(finalTable.date = 'a', '', CONCAT(e.emp_firstname, ' ', e.emp_middle_name, ' ', e.emp_lastname)) as eName,
			                    finalTable.date as date,
			                    finalTable.type as type,
			                    finalTable.number as number
			                FROM
			                    (
			
			                    SELECT
			                        l.emp_number as eID,        
			                        IF(l.date IS NULL, '', l.date) as date,
			                        IF(lt.id IS NULL, '', IF(lt.exclude_in_reports_if_no_entitlement = 1,'Un-Payment', 'Payment')) as type,
			                        ROUND(IF(l.length_hours * 0.125 IS NULL, 0, l.length_hours * 0.125), 2) as number
			                    FROM
			                        ohrm_leave as l
			                        LEFT JOIN
			                            ohrm_leave_type as lt
			                        ON
			                            l.leave_type_id = lt.id
			                        WHERE
			                            IF(l.length_hours * 0.125 IS NULL, 0, l.length_hours * 0.125) <> 0
										AND l.status > 0
			                       		AND l.date BETWEEN :from_date AND :to_date
			                    UNION
			
			                    SELECT
			                        l.emp_number as eID,
			                        'a' as date,
			                        IF(lt.id IS NULL, '', IF(lt.exclude_in_reports_if_no_entitlement = 1, 'Un-Payment', 'Payment')) as type,
			                        SUM(IF(l.length_hours * 0.125 IS NULL, 0, l.length_hours * 0.125)) as number
			                    FROM
			                        ohrm_leave as l
			                        LEFT JOIN
			                            ohrm_leave_type as lt
			                        ON
			                            l.leave_type_id = lt.id
			                        WHERE
			                            IF(l.length_hours * 0.125 IS NULL, 0, l.length_hours * 0.125) <> 0
										AND l.status > 0
			                         	AND l.date BETWEEN :from_date AND :to_date
			                    GROUP BY
			                        eID, type    
			                    ) as finalTable
			                    LEFT JOIN
			                        hs_hr_employee as e
			                    ON
			                        e.emp_number = finalTable.eID
			                ORDER BY
			                    eID, type , date     
			            ) as t1
			            LEFT JOIN
			                hs_hr_employee as e
			            ON
			                e.emp_number = t1.eID,
			            (
			                SELECT @n := 0
			            ) as stt
			        WHERE
			        	IF (:eID = '', 1 = 1, t1.eID = :eID)
			        	AND IF(:type = '', 1 = 1, t1.type = :type)
			    ) as leaveTable
			ORDER BY
			nameAo, No";
		return $q;
	}
}

?>