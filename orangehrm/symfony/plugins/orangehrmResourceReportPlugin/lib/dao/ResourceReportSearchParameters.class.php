<?php
/**
 * 
 * @author Vo Tien Thieu
 * @name: ResourceReportSearchParameter
 * 2015-07-22
 * @lastmodified : Luu Quoc Bao
 * @Date : 12/08/2015
 * @description :
 * Create a variable of this class type to hold user input get from form ( such as :
 *      + User input text
 *      + User choose sort field
 *      + User select date-time...
 * )
 * Use get-set method to interact with elements of this class
 * 
 */
class ResourceReportSearchParameters {
	private $fromDate;
	private $toDate;
	private $employee; //for set Default Data To Widgets 
    private $employeeName;
	private $employeeNumber;
	private $projectName;
	private $offset = 0;
	private $limit = 50;
	private $sortOrder = 'ASC';
	private $sortField = 'EmployeeName';
    private $leaveType;
    private $totalTime;
	
	
	public function setFromDate($fromDate) {
		if (! empty ($fromDate ))
			$this->fromDate = $fromDate;
	}
	public function setToDate($toDate) {
		if (! empty ( $toDate ))
			$this->toDate = $toDate;
	}
	public function setEmployee($employee){
		$this->employee = $employee;
	}
    public function setEmployeeName($employeeName){
        $this->employeeName = $employeeName;
    }
	public function setEmployeeNumber($employeeNumber) {
		$this->employeeNumber = $employeeNumber;
	}
	public function setProjectName($projectName) {
		$this->projectName = $projectName;
	}
	public function setOffset($offset) {
		$this->offset = $offset;
	}
	public function setLimit($limit) {
		$this->limit = $limit;
	}
    public function setLeaveType($leaveType){
        $this->leaveType = $leaveType;
    }
    public function setTotalTime($totalTime){
        $this->totalTime = $totalTime;
    }
	/**
	 * Set sortField
	 * @author Vo Tien Thieu
	 * @param $sortField
	 */
	public function setSortField($sortField) {
		if (! empty ( $sortField )) {
			$this->sortField = $sortField;
		}
	}
	/**
	 * set sortOrder	 
	 * @author Vo Tien Thieu
	 * @param unknown $sortOrder
	 */
	public function setSortOrder($sortOrder) {
		if (! empty ( $sortOrder )) {
			$this->sortOrder = $sortOrder;
		}
	}
	public function getFromDate() {
		return $this->fromDate;
	}
	public function getToDate() {
		return $this->toDate;
	}
	/**
	 * @author VoTienThieu
	 * created: 8:30. 24/07/2015
	 */
	public function getEmployee(){
		return $this->employee;
	}
    public function getEmployeeName(){
        return $this->employeeName;
    }
	public function getEmployeeNumber() {
		return $this->employeeNumber;
	}
	public function getProjectName() {
		return $this->projectName;
	}
	public function getOffset() {
		return $this->offset;
	}
	public function getLimit() {
		return $this->limit;
	}
    public function getLeaveType(){
        return $this->leaveType;
    }
    public function getTotalTime(){
        return $this->totalTime;
    }
	/**
	 * Created: 8:15, 24/07/2015
	 * @author Vo Tien Thieu
	 * @return $sortField
	 */
	public function getSortField() {
		return $this->sortField;
	}
	/**
	 * Created: 8:15, 24/07/2015
	 * @author Vo Tien THieu
	 * @return $sortOrder
	 */
	public function getSortOrder() {
		return $this->sortOrder;
	}
}