<?php
 /**
 * 
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_Form
 * 2015-07-22
 * @description :
 * Configuration class of Form to get user input.
 * 
 */
class EngineerResourceUtilization_Form extends sfForm {
	private $resourceReportService;
	private $timesheetService;
    
	public function configure() {
		$this->setWidgets ( $this->getFormWidgets () );
		$this->setValidators ( $this->getFormValidators () );
		
		$this->getWidgetSchema ()->setNameFormat ( 'resourceReport[%s]' );
		$this->getWidgetSchema ()->setLabels ( $this->getFormLabels () );
        $this->setDefaultDateForFirstCall();
	}
	
    /**
     * @author Luu Quoc Bao
     * @description other class can call this function to get Name of this form.
     */
	public function getTitle() {
		return 'EngineerResourceUtilization_Form';
	}
    
	/**
     * @Author: Luu Quoc Bao
	 * @Name : getResourceReportService
	 * @Description : get ResourceService to work with class DAO
	 * @return resourceReportService
	 */
	public function getResourceReportService(){
		if (is_null ( $this->resourceReportService )) {
			$this->resourceReportService = new ResourceReportService ();
			$this->resourceReportService->setResourceReportDao ( new ResourceReportDao () );
		}
		return $this->resourceReportService;
	}
	
    /**
     * @Author: Luu Quoc Bao
	 * @Name : getFormWidgets
	 * @Description : set Widgets for form
	 * @return $widgets
	 */
	protected function getFormWidgets() {
		$projectList = $this->getProjectList();
		
		$widgets = array (
				'employeeName' => new ohrmWidgetEmployeeNameAutoFill(array('loadingMethod'=>'ajax')),
				'projectName' =>  new sfWidgetFormSelect(array('choices' => $projectList)),
                'reportFromDate' => new ohrmWidgetDatePicker (array(), array(
                        'id' => 'report_fromDate'
                )),
                'reportToDate' => new ohrmWidgetDatePicker (array(), array(
                        'id' => 'report_toDate'
                )),
                'totalTime' => new sfWidgetFormInput(array('default' => 162),array('type' => 'number', 'min' => 1, 'max' => 1000000))
                	
		);
		
		return $widgets;
	}
    /**
     * @Author: Luu Quoc Bao
	 * @Name : getFormValidators
	 * @Description : get validator for form.
	 * @return $validators
	 */
	protected function getFormValidators() {
		$inputDatePattern = sfContext::getInstance ()->getUser ()->getDateFormat ();
		
		$validators = array (
				'employeeName' => new ohrmValidatorEmployeeNameAutoFill(),
				'projectName' => new sfValidatorString(array('required' => false)),
                'reportFromDate' => new ohrmDateValidator (array(
                        'date_format' => $inputDatePattern,
				        'required' => true
                )),
                'reportToDate' => new ohrmDateValidator (array(
                        'date_format' => $inputDatePattern,
				        'required' => true
                )),
                'totalTime' => new sfValidatorNumber() 
		);
		
		return $validators;
	}
    /**
     * @Author: Luu Quoc Bao
	 * @Name : getFormLabels
	 * @Description : get labels for form
	 * @return $labels
	 */
	protected function getFormLabels() {
		$requiredMarker = ' <em>*</em>';
		
		$labels = array (
				'employeeName' => __('Employee Name'),
				'projectName' => __('Project Name'),
                'reportFromDate' => __ ('From Date'.$requiredMarker),
                'reportToDate' => __ ('To Date'.$requiredMarker),
                'totalTime' => __ ('Total Time'.$requiredMarker) 
		);
		
		return $labels;
	}
	
	/**
     * @Author: Luu Quoc Bao
	 * Returns job Title List
	 * @return array
	 */
	private function getProjectList() {   
        $projectList = $this->getResourceReportService()->getProjectNameList();   
		$list = array("" => __('All'));
		foreach ( $projectList as $project ) {
			$list[$project['projectName']] = $project['projectName'];
		}
		return $list;
	}
    /**
     * @author Vo Tien Thieu
     * Created: 8:30, 24/07/2015
     * @param ResourceReportSearchParameters $searchParam
     * @todo setDefaultDataToWidgets
     * 
     */
    public function setDefaultDataToWidgets(ResourceReportSearchParameters $searchParam) {
    	$inputDatePattern = sfContext::getInstance ()->getUser ()->getDateFormat ();
    	 
    	$newSearchParam = new ResourceReportSearchParameters();
    	$emp = $this->getValue ( 'employeeName' );
    
    	$displayFromDate = ($searchParam->getFromDate() == $newSearchParam->getFromDate()) ? "" : $searchParam->getFromDate();
    	$displayToDate = ($searchParam->getToDate() == $newSearchParam->getToDate()) ? "" : $searchParam->getToDate();
    	$displayEmployeeName = ($searchParam->getEmployeeNumber() == $emp['empId']) ? "" : $searchParam->getEmployee();
    	$displayProject = ($searchParam->getProjectName() == $newSearchParam->getProjectName()) ? "" : $searchParam->getProjectName();
    	
    	
    	$this->setDefault('employeeName', $displayEmployeeName);
    	$this->setDefault('reportFromDate', date($inputDatePattern, strtotime($searchParam->getFromDate())));
        $this->setDefault('reportToDate', date($inputDatePattern, strtotime($searchParam->getToDate())));
    	$this->setDefault('projectName', $displayProject);
        $this->setDefault('totalTime', 162);
    }
    
    /**
     * @Author: Luu Quoc Bao
     * Last Modified: 8:30, 24/07/2015 by Vo Tien Thieu
     * @param ResourceReportSearchParameters $searchParam
     */
	public function getResourceReportSearchParams( ResourceReportSearchParameters $searchParam ) {
        $report_fromDate = $this->getValue( 'reportFromDate' );
        $report_toDate = $this->getValue( 'reportToDate' );
		$searchParam->setFromDate( $report_fromDate );
		$searchParam->setToDate($report_toDate);
		$emp = $this->getValue( 'employeeName' );
        if($emp['empName'] == ''){
            $emp['empId'] = '';
        }
        else{
            //do nothing
        }
        $searchParam->setEmployee($emp);
		$searchParam->setEmployeeNumber( $emp['empId'] );  
		$project = $this->getValue( 'projectName' );
        $searchParam->setProjectName( $project );
        $totalTime = $this->getValue( 'totalTime' );
        $searchParam->setTotalTime($totalTime);       
        return $searchParam;
	}
    
    /**
     * @Author: Luu Quoc Bao
     * @Description: set default for Date-Time picker.
     */
    
     public function setDefaultDateForFirstCall(){
        $inputDatePattern = sfContext::getInstance ()->getUser ()->getDateFormat ();
        $offset = date(d) - 01;
        $today = date($inputDatePattern);
        $firstday = date($inputDatePattern,strtotime("-".$offset." days"));
        $this->setDefault('reportFromDate', $firstday );
        $this->setDefault('reportToDate', $today);       
    }
    
    /**
     * @Author: Vo Tin Thieu
     * @LastModified: 8:30, 24/07/2015 by Vo Tien Thieu
     * @Description: return list of Employee Name for auto complete text box on form.
     */
    public function getEmployeeListAsJson($workShift = false) {
    	try {
    		$jsonString = array();
    		$q = Doctrine_Query :: create()->from('Employee');
    		$employeeList = $q->execute();
    
    		foreach ($employeeList as $employee) {
    			$workShiftLength = 0;
    			if ($workShift) {
    				$employeeWorkShift = $this->getEmployeeWorkShift($employee->getEmpNumber());
    				if ($employeeWorkShift != null) {
    					$workShiftLength = $employeeWorkShift->getWorkShift()->getHoursPerDay();
    				} else
    					$workShiftLength = WorkShift :: DEFAULT_WORK_SHIFT_LENGTH;
    			}
    			$terminationId = $employee->getTerminationId();
    			if (empty($terminationId)) {
                    if($employee->getMiddleName() !== ''){
                        $name = $employee->getFirstName() . " " . $employee->getMiddleName() . " " . $employee->getLastName();
                    } else{
                        $name = $employee->getFirstName() . " " . $employee->getLastName();
                    }                  
                    $jsonString[] = array('name' => $name, 'id' => $employee->getEmpNumber());
    			}
    		}
    
    		$jsonStr = json_encode($jsonString);
    		return $jsonStr;
    		// @codeCoverageIgnoreStart
    	} catch (Exception $e) {
    		throw new DaoException($e->getMessage(), $e->getCode(), $e);
    	}
    	// @codeCoverageIgnoreEnd
    }
}

?>