<?php
/**
 * @author : Luu Quoc Bao
 * @name   : ResourceReport Service
 * @description:
 *      A Mediate class, this class help Action class interact with Dao class.
 *      Action class can call function of service to use DAO(Data access object) to get data from databae. 
 *  
*/ 

class ResourceReportService extends BaseService {
	protected  $resourceReportDao;
	
	/**
	 * Construct
	 */
	public function __construct(){
		$this->resourceReportDao = new ResourceReportDao();
	}

	public function getResourceReportDao(){
		return $this->resourceReportDao;
	}
    
	public function setResourceReportDao(ResourceReportDao $resourceReportDao){
		return $this->resourceReportDao = $resourceReportDao;
	}
    
    /**
     *@Author : Luu Quoc Bao
     *@Description : Each function below help class Action to :
     *               call a corresponding function in class ResourceReportDAO
     *  
     */

	public function getEngineerResourceUtilizationDetail_Data(ResourceReportSearchParameters $para) {
		return $this->getResourceReportDao()->getEngineerResourceUtilizationDetail_Data($para);
	}

	public function getProjectResourceUtilization_Data(ResourceReportSearchParameters $para) {
		return $this->getResourceReportDao()->getProjectResourceUtilization_Data($para);
	}

	public function getTimeSheet_Data(ResourceReportSearchParameters $para) {
		return $this->getResourceReportDao()->getTimeSheet_Data($para);
	}
    
     public function getProjectNameList(){
        return $this->getResourceReportDao()->getProjectNameList();
     }
	 
	 public function getLeaveReport_Data(ResourceReportSearchParameters $para){
		return $this->getResourceReportDao()->getLeaveReport_Data($para);
	 }

}
?>