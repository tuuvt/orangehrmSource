<?php
 /**
 * Last Modified: 8:30, 24/07/2015, by Vo Tien Thieu
 * Edit for sorting
 *
 *
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_TTVHeaderFactory
 * 2015-07-22
 * @description :
 * Class that hold configuration for list to show result from resourceRService
 */
class EngineerResourceUtilization_TTVHeaderFactory extends ohrmListConfigurationFactory {
	  
protected function init() {

		$headerList = array();

		for ($i = 0; $i < 7; $i++) {
			$headerList[$i] = new ListHeader();
		}

        $headerList[0]->populateFromArray(array(
				'name' => 'Employee\'s Name',
				'width' => '15%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'EmployeeName',
				'elementProperty' => array('getter' => 'EmployeeName'),
		));
		
        $headerList[1]->populateFromArray(array(
				'name' => 'Project role',
				'width' => '10%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'Project_role',
				'elementProperty' => array('getter' => 'Project_role'),
		));
        
        $headerList[2]->populateFromArray(array(
        		'name' => 'Target',
        		'width' => '10%',
        		'elementType' => 'label',
        		'elementProperty' => array('getter' => 'Target'),
        ));
        
        $headerList[3]->populateFromArray(array(
        		'name' => 'Actual (%)',
        		'width' => '10%',
        		'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' => 'percent',
        		'elementProperty' => array('getter' => 'ActualPercent'),
        ));
        $headerList[4]->populateFromArray(array(
				'name' => 'Actual Hours',
				'width' => '10%',
				'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' => 'Hour',
				'elementProperty' => array('getter' => 'Hour'),
		));
        
        $headerList[5]->populateFromArray(array(
				'name' => 'Project',
				'width' => '20%',
				'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' =>'Project',
				'elementProperty' => array('getter' => 'Project'),
		));
		
		$headerList[6]->populateFromArray(array(
				'name' => 'Note',
				'width' => '25%',
				'elementType' => 'label',
				'elementProperty' => array('getter' => 'Note'),
		));
		
		$this->headers = $headerList;
	}

	public function getClassName() {
		return '';
	}

}

?>
