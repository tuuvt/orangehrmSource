<?php
/**
 *
 * @author Nguyen Thanh Dat
 * @name EngineerResourceUtilizationDetail_HeaderFactory
 * 22/07/2015
 * @description: Config for list of Engineer Resource Utilization report (print out list on web view)
 *
 * Last Modified: 14:10, 24/07/2015, by Nguyen Thanh Dat
 * Edit for sorting
 */
class EngineerResourceUtilizationDetail_HeaderFactory extends ohrmListConfigurationFactory {
	  
	protected function init() {

		$headerList = array();

		for ($i = 0; $i < 8; $i++) {
			$headerList[$i] = new ListHeader();
		}
		
        $headerList[0]->populateFromArray(array(
				'name' => 'Employee\'s Name',
				'width' => '16%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'EmployeeName',
				'elementProperty' => array('getter' => 'EmployeeName'),
		));

		$headerList[1]->populateFromArray(array(
				'name' => 'Bill',
				'width' => '7%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => '',
				'elementProperty' => array('getter' => 'type'),
		));
		
        $headerList[2]->populateFromArray(array(
				'name' => 'Project role',
				'width' => '10%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'Project_role',
				'elementProperty' => array('getter' => 'Project_role'),
		));
        
        $headerList[3]->populateFromArray(array(
        		'name' => 'Target',
        		'width' => '10%',
        		'elementType' => 'label',
        		'elementProperty' => array('getter' => 'Target'),
        ));
        
        $headerList[4]->populateFromArray(array(
        		'name' => 'Actual (%)',
        		'width' => '10%',
        		'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' => 'percent',
        		'elementProperty' => array('getter' => 'ActualPercent'),
        ));
        $headerList[5]->populateFromArray(array(
				'name' => 'Actual Hours',
				'width' => '10%',
				'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' => 'Hours',
				'elementProperty' => array('getter' => 'Hours'),
		));
        
        $headerList[6]->populateFromArray(array(
				'name' => 'Project',
				'width' => '20%',
				'elementType' => 'label',
        		'isSortable' => false,
        		'sortField' =>'Project',
				'elementProperty' => array('getter' => 'Project'),
		));
		
		$headerList[7]->populateFromArray(array(
				'name' => 'Note',
				'width' => '25%',
				'elementType' => 'label',
				'elementProperty' => array('getter' => 'Note'),
		));
		
		$this->headers = $headerList;
	}

	public function getClassName() {
		return '';
	}

}

?>
