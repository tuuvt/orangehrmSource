<?php
/**
*@author: Nguyen Thanh Dat
*@name  : ProjectResourceUtilization_TCIHeaderFactory
*@date  : 2015-07-22
*@todo    : Config list for TimeSheetDetail Resource Utilization report (print out list on web view)
*
* Last Modified: 15:30, 24/07/2015, by Nguyen Thanh Dat
* Edit for sorting
*/

class TimeSheetDetails_HeaderFactory extends ohrmListConfigurationFactory {
	protected function init(){
		
		$header1 = new ListHeader();
		$header2 = new ListHeader();
 	    $header3 = new ListHeader();
        $header4 = new ListHeader();
        $header5 = new ListHeader();
        $header6 = new ListHeader();
        
        $header1->populateFromArray(array(
				'name' => 'Employee\'s Name',
				'width' => '18%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'EmployeeName',
				'elementProperty' => array('getter' => 'EmployeeName'),
		));
		
		$header2->populateFromArray(array(
				'name' => 'Count for Project',
				'width' => '12%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'CountForProject',
				'elementProperty' => array('getter' => 'CountForProject'),
		));
		
		$header3->populateFromArray(array(
				'name' => 'Project',
				'width' => '20%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'Project',
				'elementProperty' => array('getter' => 'Project'),
		));
		
		$header4->populateFromArray(array(
				'name' => 'Activities',
				'width' => '20%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'Note',
				'elementProperty' => array('getter' => 'Note'),
		));
		
		$header5->populateFromArray(array(
				'name' => 'Hours',
				'width' => '10%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'Hours',
				'elementProperty' => array('getter' => 'Hours'),
		));
		$header6->populateFromArray(array(
				'name' => 'ODC',
				'width' => '15%',
				'elementType' => 'label',
        		'isSortable' => false,
		    	'sortField' => 'ODC',
				'elementProperty' => array('getter' => 'ODC'),
		));
		
		$this->headers = array($header1,$header2,$header3,$header4,$header5,$header6);
	}
	public function getClassName() {
		return '';
	}
}
?>