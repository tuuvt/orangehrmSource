<?php
/**
*@author: Nguyen Thanh Dat
*@name  : ProjectResourceUtilization_TCIHeaderFactory
*@date  : 2015-07-22
*@todo    : Config list for Project Resource Utilization report (print out list on web view)
*
* Last Modified: 15:30, 24/07/2015, by Nguyen Thanh Dat
* Edit for sorting
*/
	class ProjectResourceUtilization_HeaderFactory extends ohrmListConfigurationFactory
	{
		protected function init()
		{
			$headerList = array();

			for ($i = 0; $i < 9; $i++) {
				$headerList[$i] = new ListHeader();
			}
            
            $headerList[0]->populateFromArray(array(
					'name' => 'Bill',
					'width' => '5%',
					'elementType' => 'label',
					'isSortable' => false,
        			'sortField' =>'NameOfProject',
					'elementProperty' => array('getter' => 'type'),
			));


	        $headerList[1]->populateFromArray(array(
					'name' => 'Name of Project',
					'width' => '25%',
					'elementType' => 'label',
					'isSortable' => false,
        			'sortField' =>'NameOfProject',
					'elementProperty' => array('getter' => 'NameOfProject'),
			));
			
	        $headerList[2]->populateFromArray(array(
					'name' => 'Request By',
					'width' => '10%',
					'elementType' => 'label',
					'elementProperty' => array('getter' => 'RequestBy'),
			));
	        
	        $headerList[3]->populateFromArray(array(
	        		'name' => 'Target',
	        		'width' => '10%',
	        		'elementType' => 'label',
	        		'elementProperty' => array('getter' => 'Target'),
	        ));
	        
	        $headerList[4]->populateFromArray(array(
	        		'name' => 'Actual (%)',
	        		'width' => '10%',
	        		'elementType' => 'label',
	        		'isSortable' => false,
        			'sortField' =>'percent',
	        		'elementProperty' => array('getter' => 'ActualPercent'),
	        ));

	        $headerList[5]->populateFromArray(array(
					'name' => 'Actual Hours',
					'width' => '10%',
					'elementType' => 'label',
					'isSortable' => false,
        			'sortField' =>'ActualHours',
					'elementProperty' => array('getter' => 'ActualHours'),
			));
	        
	        $headerList[6]->populateFromArray(array(
					'name' => 'Project Head Count',
					'width' => '12%',
					'elementType' => 'label',
					'isSortable' => false,
        			'sortField' =>'ProjectHeadCount',
					'elementProperty' => array('getter' => 'ProjectHeadCount'),
			));
			
			$headerList[7]->populateFromArray(array(
					'name' => 'Utilization Margin',
					'width' => '11%',
					'elementType' => 'label',
					'elementProperty' => array('getter' => 'UtilizationMargin'),
			));

			$headerList[8]->populateFromArray(array(
				'name' => 'Head Count',
				'width' => '11%',
				'elementType' => 'label',
				'elementProperty' => array('getter' => 'HeadCount'),
			));
			
			$this->headers = $headerList;
		}

		public function getClassName()
		{
			return '';
		}
	}

?>
