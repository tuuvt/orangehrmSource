<?php

/**
 * 
 * @author Nguyen Minh Cuong
 * @name: ProjectResourceUtilization_TTVHeaderFactory.
 * 2015-07-22
 * Header Factory for report Project Resource Utilization - TTV
 */
class ProjectResourceUtilization_TTVHeaderFactory extends ohrmListConfigurationFactory {
	protected function init() {
		$header1 = new ListHeader ();
		$header2 = new ListHeader ();
		$header3 = new ListHeader ();
		$header4 = new ListHeader ();
		$header5 = new ListHeader ();
		$header6 = new ListHeader ();
		$header7 = new ListHeader ();
		$header8 = new ListHeader ();
		
		$header1->populateFromArray ( array (
				'name' => 'Name of Project',
				'width' => '22%',
				'elementType' => 'label',
				'isSortable' => true,
				'sortField' => 'ProjectName',
				'elementProperty' => array (
						'getter' => 'ProjectName' 
				) 
		) );
		
		$header2->populateFromArray ( array (
				'name' => 'Request By',
				'width' => '10%',
				'elementType' => 'label',
				'elementProperty' => array (
						'getter' => 'RequestBy' 
				) 
		) );
		
		$header3->populateFromArray ( array (
				'name' => 'Target',
				'width' => '10%',
				'elementType' => 'label',
				'elementProperty' => array (
						'getter' => 'Target' 
				) 
		) );
		
		$header4->populateFromArray ( array (
				'name' => 'Actual (%)',
				'width' => '10%',
				'elementType' => 'label',
				'isSortable' => true,
				'sortField' => 'percent',
				'elementProperty' => array (
						'getter' => 'ActualPercent' 
				) 
		) );
		$header5->populateFromArray ( array (
				'name' => 'Actual Hours',
				'width' => '10%',
				'elementType' => 'label',
				'isSortable' => true,
				'sortField' => 'ActualHour',
				'elementProperty' => array (
						'getter' => 'ActualHour' 
				) 
		) );
		
		$header6->populateFromArray ( array (
				'name' => 'Project Head Count',
				'width' => '12%',
				'elementType' => 'label',
				'isSortable' => true,
				'sortField' => 'ProjectHeadCount',
				'elementProperty' => array (
						'getter' => 'ProjectHeadCount' 
				) 
		) );
		
		$header7->populateFromArray ( array (
				'name' => 'Utilization Margin',
				'width' => '15%',
				'elementType' => 'label',
				'elementProperty' => array (
						'getter' => 'UtilizationMargin' 
				) 
		) );
		
		$header8->populateFromArray ( array (
				'name' => 'Head Count',
				'width' => '15%',
				'elementType' => 'label',
				'elementProperty' => array (
						'getter' => 'HeadCount' 
				) 
		) );
		
		$this->headers = array (
				$header1,
				$header2,
				$header3,
				$header4,
				$header5,
				$header6,
				$header7,
				$header8 
		);
	}
	public function getClassName() {
		return '';
	}
}

?>
