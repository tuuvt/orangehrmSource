<?php
/**
*@author: Vo Tien Thieu
*@name  : ProjectResourceUtilization_TCIHeaderFactory
*@date  : 2015-07-22
*@todo    : Config list for Project Leave report (print out list on web view)
*
* Last Modified: 15:30, 24/07/2015, by Nguyen Thanh Dat
* Edit for sorting
*/
	class LeaveReport_HeaderFactory extends ohrmListConfigurationFactory
	{
		protected function init()
		{
			$headerList = array();

            for ($i = 1; $i < 5; $i++) {
				$headerList[$i] = new ListHeader();
			}
            $headerList[1]->populateFromArray(array(
    				'name' => 'Employee\'s Name',
    				'width' => '15%',
    				'elementType' => 'label',
            		'isSortable' => false,
    		    	'sortField' => 'eName',
    				'elementProperty' => array('getter' => 'eName'),
    		));
    		
            $headerList[2]->populateFromArray(array(
    				'name' => 'Date',
    				'width' => '10%',
    				'elementType' => 'label',
            		'isSortable' => false,
    		    	'sortField' => 'date',
    				'elementProperty' => array('getter' => 'date'),
    		));
            
            $headerList[3]->populateFromArray(array(
            		'name' => 'Type',
            		'width' => '10%',
            		'elementType' => 'label',
            		'elementProperty' => array('getter' => 'type'),
            ));
            
            $headerList[4]->populateFromArray(array(
            		'name' => 'Number of Days',
            		'width' => '10%',
            		'elementType' => 'label',
            		'isSortable' => false,
            		'sortField' => 'numDay',
            		'elementProperty' => array('getter' => 'numDay'),
            ));
    		
    		$this->headers = $headerList;
		}

		public function getClassName()
		{
			return '';
		}
	}

?>
