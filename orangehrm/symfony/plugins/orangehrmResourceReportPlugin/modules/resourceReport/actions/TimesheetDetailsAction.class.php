<?php
/**
 * @author Nguyen Nhat Han
 * @name : ViewTemplateAction
 *       2015-07-22
 *       actions for TimeSheetDetailsAction
 * @lastModified : By Luu Quoc Bao
 * @reason : Extend BaseResourceReport action to reduce code
 */
class TimesheetDetailsAction extends BaseResourceReportAction {
	/**
	 * execute
	 *
	 * @param
	 *        	$request
	 * @return $response
	 */
	public function execute($request) {
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'viewPageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'viewPageNumber' );
		}
		$this->getUser ()->setAttribute ( 'viewPageNumber', $pageNumber );
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setLimit ( sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		// ////////////////////////////////////////////////////////////////////////
		// Create Form to get User Input
		$this->setForm ( new TimeSheetDetail_Form() );
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'viewSearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'viewSearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'viewSearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'VT';
		$timeSheetDetails_Data = $this->getResourceReportService ()->getTimeSheet_Data ( $searchParam );
		$this->setListComponent ( $timeSheetDetails_Data ['data'], $timeSheetDetails_Data ['count'], $pageNumber, $noOfRecords, $reportType );
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'viewPageNumber', $pageNumber );
				
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				if ($this->form->isValid ()) {
					$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
					$this->getUser ()->setAttribute ( 'viewSearchParameters', $searchParam );
					
					if ($_POST ['exportExcel'] == 1) {
						// get all data for export
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 'EmployeeName' );
						$srchForExport->setLimit ( 0 );
						$exportData = $this->getResourceReportService ()->getTimeSheet_Data ( $srchForExport );
						$this->exportExcel_TsD ( $srchForExport, $exportData );
						return;
					}
					
					$timeSheetDetails_Data = $this->getResourceReportService ()->getTimeSheet_Data ( $searchParam );
					$this->setListComponent ( $timeSheetDetails_Data ['data'], $timeSheetDetails_Data ['count'], $pageNumber, $noOfRecords, $reportType );
				}
			}
		}
	}
	/**
	 * export excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Timesheet Details.xls
	 */
	public function exportExcel_TsD($srchForExport, $exportData) {
		$object = new exportExcel ();
		$object->setTypeReport ( 3 );
		$this->checkType = $object->getTypeReport ();
		// check type report
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3 || $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $exportData ['count'];
			if ($this->checkCount != 0) {
				$data = array ();
				$title = array (
						"Employee's Name",
						'Count for Project',
						'Project',
						"Activities",
						'Hours',
						'ODC' 
				);
				foreach ( $exportData ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [0],
							$title [1] => $a [1],
							$title [2] => $a [2],
							$title [3] => $a [3],
							$title [4] => $a [4],
							$title [5] => $a [5] 
					) );
				}
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $srchForExport->getFromDate () . ' to ' . $srchForExport->getToDate () );
				$object->setNameReport ( 'Timesheet Details' );
				$object->setFileName ( 'Timesheet Details' );
				$object->createReport ( $title, $data );
			}
		}
	}
}