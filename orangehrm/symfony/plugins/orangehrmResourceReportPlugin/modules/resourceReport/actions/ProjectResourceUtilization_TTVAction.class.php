<?php
/**
 *
 * @author Nguyen Minh Cuong
 * @name: ProjectResourceUtilization_TTVAction.
 * 2015-07-22
 * Action for report Project Resource Utilization - TTV
 * Last Modified: 15:10, 29/07/2015, by Luu Quoc Bao
 * Reason: extend base resource report class to reduce code
 */
class ProjectResourceUtilization_TTVAction extends BaseResourceReportAction {
	/**
	 * execute report Engineer Resource Utilization - TCI
	 *
	 * ription :
	 * Create a form to get user input
	 * Call Query from resourceReportService to get data for report Engineer Resource Ultilization - TCI
	 * Query's result keep in $EngineerResourceUtilization_TCI_Data when catch method post
	 * Create a list to show $EngineerResourceUtilization_TCI_Data.
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$request
	 * @return Call file EngineerResouceUltilization_TCISuccess
	 */
	public function execute($request) {
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setSortField ( 't1.ProjectName' );
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'PRUpageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'PRUpageNumber' );
		}
		$this->getUser ()->setAttribute ( 'PRUpageNumber', $pageNumber );
		// set for test
		$searchParam->setLimit ( sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		
		// ////////////////////////////////////////////////////////////////////////
		// Create Form to get User Input
		$this->setForm ( new ProjectResourceUtilization_Form () );
		
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'PRUsearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'PRUsearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'PRUsearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'PRU_TTV';
		$ProjectResourceUtilization_TTV_Data = $this->getResourceReportService ()->getProjectResourceUtilization_TTV_Data ( $searchParam );
		$this->setListComponent ( $ProjectResourceUtilization_TTV_Data ['data'], $ProjectResourceUtilization_TTV_Data ['count'], $pageNumber, $noOfRecords, $reportType );
		
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'ERUpageNumber', $pageNumber );
				
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				if ($this->form->isValid ()) {
					$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
					$this->getUser ()->setAttribute ( 'ERUsearchParameters', $searchParam );
					
					if ($_POST ['exportExcel'] == 1) {
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 't1.ProjectName' );
						$srchForExport->setLimit ( 0 );
						$exportData = $this->getResourceReportService ()->getProjectResourceUtilization_TTV_Data ( $srchForExport );
						$this->exportExcel_PRU_TTV ( $srchForExport, $exportData );
						return;
					}
				}
				$ProjectResourceUtilization_TTV_Data = $this->getResourceReportService ()->getProjectResourceUtilization_TTV_Data ( $searchParam );
				$count = $ProjectResourceUtilization_TTV_Data ['count'];
				$this->setListComponent ( $ProjectResourceUtilization_TTV_Data ['data'], $count, $pageNumber, $noOfRecords, $reportType );
			}
		}
	}
	/**
	 * export excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Project Resource Utilization Report TTV.xls
	 */
	public function exportExcel_PRU_TTV($searchParam, $exportData) {
		$object = new exportExcel ();
		$object->setTypeReport ( 2 );
		$this->checkType = $object->getTypeReport ();
		// check type report
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3|| $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $exportData ['count'];
			if ($this->checkCount != 0) {
				$data = array ();
				$title = array (
						"Name of project",
						'Request By',
						'Target',
						"Actual (%)",
						'Actual Hours',
						'Project head count',
						'Utilization Margin',
						'Head count' 
				);
				foreach ( $exportData ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [0],
							$title [1] => $a [1],
							$title [2] => $a [2],
							$title [3] => $a [3],
							$title [4] => $a [4],
							$title [5] => $a [5],
							$title [6] => $a [6],
							$title [7] => $a [7] 
					) );
				}
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $searchParam->getFromDate () . ' to ' . $searchParam->getToDate () );
				$object->setNameReport ( 'Project Resource Utilization TTV' );
				$object->setFileName ( 'Project Resource Utilization Report TTV' );
				$object->createReport ( $title, $data );
			}
		}
	}
}
?>