<?php
/**
 *
 * @author Luu Quoc Bao
 * @name: BaseResourceReportAction
 * 2015-07-29
 * Contain basic function for all action class.
 */
abstract class BaseResourceReportAction extends sfAction {

    private $baseResourceReportService;
    
    /**
	 * get ResourceReport Service to use
	 *
	 * @author Luu Quoc Bao
	 * @param
	 *        	$request
	 * @return $respone
	 */
    protected function getResourceReportService() {
		if (is_null ( $this->resourceReportService )) {
			$this->baseResourceReportService = new ResourceReportService ();
		}
		return $this->baseResourceReportService;
	}
    
    /**
	 * Set form for template to get user input
	 *
	 * @author Luu Quoc Bao
	 * @param
	 * 
	 * @return $this->form is set
	 */
    protected function setForm(sfForm $form) {
		if (is_null ( $this->form )) {
			$this->form = $form;
		}
	}
    
    /**
	 * Set Configuration for the list that show result in $EngineerResourceUtilization_TTV_Data
	 *
	 * @author Luu Quoc Bao
	 * @param
	 *        	$request
	 * @return $respone
	 */
    protected function setListComponent($List, $count, $pageNumber, $noOfRecords, $reportType) {
        switch ($reportType){
            case'ERU_TTV':              
                $configurationFactory = new EngineerResourceUtilization_TTVHeaderFactory ();
                break;
            case'ERUD':
                $configurationFactory = new EngineerResourceUtilizationDetail_HeaderFactory ();
                break;
            case'PRU_TTV':
                $configurationFactory = new ProjectResourceUtilization_TTVHeaderFactory ();
                break;
            case'PRU':
                $configurationFactory = new ProjectResourceUtilization_HeaderFactory ();
                break;
            case'VT':
                $configurationFactory = new TimeSheetDetails_HeaderFactory ();
                break;
            case 'LR':
                $configurationFactory = new LeaveReport_HeaderFactory();
                break;
            	
        }
		$runtimeDefinitions = array ();
		$configurationFactory->setRuntimeDefinitions ( $runtimeDefinitions );
		ohrmListComponent::setConfigurationFactory ( $configurationFactory );
		ohrmListComponent::setActivePlugin ( 'orangehrmResourceReportPlugin' );
		ohrmListComponent::setListData ( $List );
		ohrmListComponent::setItemsPerPage ( $noOfRecords );
		ohrmListComponent::setPageNumber ( $pageNumber );
		ohrmListComponent::setNumberOfRecords ( $count );
	}
}