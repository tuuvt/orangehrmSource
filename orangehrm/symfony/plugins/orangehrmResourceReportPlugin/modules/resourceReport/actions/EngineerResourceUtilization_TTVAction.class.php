<?php
/**
 * @author Luu Quoc Bao
 * @name : EngineerResourceUtilization_TTVAction
 *       2015-07-22
 *       Action for report Engineer Resource Utilization - TTV
 * Last Modified: 11:30, 29/07/2015, by Luu Quoc Bao
 * extend base resource action to reduce code.
 */
class EngineerResourceUtilization_TTVAction extends BaseResourceReportAction {
	/**
	 * function excute when client send request to server on this module.
	 * Description :
	 * Create a form to get user input
	 * Call Query from resourceReportService to get data for report Engineer Resource Ultilization - TTV
	 * Query's result keep in $EngineerResourceUtilization_TTV_Data when catch method post
	 * Create a list to show $EngineerResourceUtilization_TTV_Data.
	 *
	 * @author Luu Quoc Bao
	 * @param
	 *        	$request
	 * @return Call file EngineerResouceUltilization_TTVSuccess
	 */
	public function execute($request) {
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'ERUpageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'ERUpageNumber' );
		}
		$this->getUser ()->setAttribute ( 'ERUpageNumber', $pageNumber );
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setLimit (sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		$this->setForm ( new EngineerResourceUtilization_Form () ); // Create Form to get User Input
		
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'ERUsearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'ERUsearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'ERUsearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'ERU_TTV';
		$EngineerResourceUtilization_TTV_Data = $this->getResourceReportService ()->getEngineerResourceUtilization_TTV_Data ( $searchParam );
		$this->setListComponent ( $EngineerResourceUtilization_TTV_Data ['data'], $EngineerResourceUtilization_TTV_Data ['count'], $pageNumber, $noOfRecords, $reportType );
		// ////////////////////////////////////////////////////////////////////////
		// Catch method post from client to call query and show result
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'ERUpageNumber', $pageNumber );
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				if ($this->form->isValid ()) {
					$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
					$this->getUser ()->setAttribute ( 'ERUsearchParameters', $searchParam );
					if ($_POST ['exportExcel'] == 1) {
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 'EmployeeName' );
						$srchForExport->setLimit ( 0 );
						$Export_data = $this->getResourceReportService ()->getEngineerResourceUtilization_TTV_Data ( $srchForExport );
						$this->exportExel_ERU_TTV ( $srchForExport, $Export_data );
						return;
					}
					$EngineerResourceUtilization_TTV_Data = $this->getResourceReportService ()->getEngineerResourceUtilization_TTV_Data ( $searchParam );
					$count = $EngineerResourceUtilization_TTV_Data ['count'];
						
					$this->setListComponent ( $EngineerResourceUtilization_TTV_Data ['data'], $count, $pageNumber, $noOfRecords, $reportType );
				}
			}
		}
	}
	/**
	 * export to file excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Engineer Resource Utilization Report TTV.xls
	 */
	public function exportExel_ERU_TTV($srchForExport, $Export_data) {
		// $CoTheExport = true;
		$object = new exportExcel ();
		$object->setTypeReport ( 1 );
		$this->checkType = $object->getTypeReport ();
		// check type report
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3 || $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $Export_data ['count'];
			if ($this->checkCount != 0) {
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $srchForExport->getFromDate () . ' to ' . $srchForExport->getToDate () );
				$object->setNameReport ( 'Engineer Resource Utilization TTV' );
				$object->setFileName ( 'Engineer Resource Utilization Report TTV' );
				$data = array ();
				$title = array (
						"Employee's Name",
						'Project role',
						'Target',
						"Actual(%)",
						'Actual Hours',
						'Project',
						'Note' 
				);
				foreach ( $Export_data ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [0],
							$title [1] => $a [1],
							$title [2] => $a [2],
							$title [3] => $a [3],
							$title [4] => $a [4],
							$title [5] => $a [5],
							$title [6] => $a [6] 
					) );
				}
				$object->createReport ( $title, $data );
			}
		} else {
			// do nothing
		}
	}
	
	
}

?>