<?php
/**
 * @author Nguyen Thanh Dat
 * @name: EngineerResourceUtilization_TCIAction
 * 2015-07-22
 * Action for report Engineer Resource Utilization Detail
 * Last Modified: 13:50, 11/08/2015, by Luu Quoc Bao
 * 
 */
class EngineerResourceUtilizationDetailAction extends BaseResourceReportAction {
	/**
	 * execute report Engineer Resource Utilization Detail
	 * Description :
	 * Create a form to get user input
	 * Call Query from resourceReportService to get data for report Engineer Resource Ultilization Detail
	 * Query's result keep in $EngineerResourceUtilizationDetail_Data when catch method post
	 * Create a list to show $EngineerResourceUtilizationDetail_Data.
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$request
	 * @return Call file EngineerResouceUltilization Detail
	 *         @lastModified : by Luu Quoc Bao
	 *       
	 */
	public function execute($request) {
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'ERUD_PageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'ERUD_PageNumber' );
		}
		$this->getUser ()->setAttribute ( 'ERUD_PageNumber', $pageNumber );
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setLimit ( sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		$this->setForm ( new EngineerResourceUtilization_Form () ); // Create Form to get User Input
		
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'ERUD_SearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'ERUD_SearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'ERUD_SearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'ERUD';
		$EngineerResourceUtilizationDetail_Data = $this->getResourceReportService ()->getEngineerResourceUtilizationDetail_Data ( $searchParam );
		$this->setListComponent ( $EngineerResourceUtilizationDetail_Data ['data'], $EngineerResourceUtilizationDetail_Data ['count'], $pageNumber, $noOfRecords, $reportType );
		// ////////////////////////////////////////////////////////////////////////
		// Catch method post from client to call query and show result
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'ERUD_PageNumber', $pageNumber );
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
				if ($this->form->isValid ()) {
					$this->getUser ()->setAttribute ( 'ERUD_SearchParameters', $searchParam );
					
					if ($_POST ['exportExcel'] == 1) {
						// get all data for export
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 'EmployeeName' );
						$srchForExport->setLimit ( 0 );
						$Export_data = $this->getResourceReportService ()->getEngineerResourceUtilizationDetail_Data ( $srchForExport );
						$this->exportExcel_ERUD( $srchForExport, $Export_data );
						return;
					}
					$EngineerResourceUtilizationDetail_Data = $this->getResourceReportService ()->getEngineerResourceUtilizationDetail_Data ( $searchParam );
					$count = $EngineerResourceUtilizationDetail_Data ['count'];
					$this->setListComponent ( $EngineerResourceUtilizationDetail_Data ['data'], $count, $pageNumber, $noOfRecords, $reportType, $reportType );
				}
			}
		}
	}
	/**
	 * export excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Engineer Resource Utilization Report Detail.xls
	 */
	public function exportExcel_ERUD(ResourceReportSearchParameters $srchForExport, $Export_data) {
		$object = new exportExcel ();
		$object->setTypeReport ( 5 );
		$this->checkType = $object->getTypeReport ();
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3 || $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $Export_data ['count'];
			if ($this->checkCount != 0) {
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $srchForExport->getFromDate () . ' to ' . $srchForExport->getToDate () );
				$object->setNameReport ( 'Engineer Resource Utilization Detail' );
				$object->setFileName ( 'Engineer Resource Utilization Detail Report ' );
				$data = array ();
				$title = array (
						"Employee's Name",
						'Bill',
						'Project role',
						'Target',
						"Actual (%)",
						'Actual Hours',
						'Project',
						'Note' 
				);
				foreach ( $Export_data ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [2],
							$title [1] => $a [3],
							$title [2] => $a [5],
							$title [3] => $a [4],
							$title [4] => $a [6],
							$title [5] => $a [7],
							$title [6] => $a [8],
							$title [7] => $a [9],
							'eID'=> $a[0]
							
					) );
				}
				$object->createReport ( $title, $data );
			}
		}
	}
}