<?php
/**
 * @author Vo Tien Thieu
 * 2015-07-22
 * Action for report Leave Report
 * Last Modified: 13:50, 29/07/2015, by Luu Quoc Bao
 * extend base resource report action to reduce code
 */
class LeaveReportAction extends BaseResourceReportAction {
	/**
	 *
	 * @author Vo Tien Thieu
	 * @param
	 *        	$request
	 */
	public function execute($request) {
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'leaveReport_PageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'leaveReport_PageNumber' );
		}
		$this->getUser ()->setAttribute ( 'leaveReport_PageNumber', $pageNumber );
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setLimit ( sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		$this->setForm ( new LeaveReport_Form () ); // Create Form to get User Input
		
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'leaveReport_SearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'leaveReport_SearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'leaveReport_SearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'LR';
		$leaveReport_data = $this->getResourceReportService ()->getLeaveReport_Data ( $searchParam );
		$this->setListComponent ( $leaveReport_data ['data'], $leaveReport_data ['count'], $pageNumber, $noOfRecords, $reportType );
		// ////////////////////////////////////////////////////////////////////////
		// Catch method post from client to call query and show result
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'leaveReport_PageNumber', $pageNumber );
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
				if ($this->form->isValid ()) {
					$this->getUser ()->setAttribute ( 'leaveReport_SearchParameters', $searchParam );
					
					if ($_POST ['exportExcel'] == 1) {
						// get all data for export
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 'EmployeeName' );
						$srchForExport->setLimit ( 0 );
						$Export_data = $this->getResourceReportService ()->getLeaveReport_Data ( $srchForExport );
						$this->exportExcel_leaveReport ( $srchForExport, $Export_data );
						return;
					}
					$leaveReport_data = $this->getResourceReportService ()->getLeaveReport_Data ( $searchParam );
					$count = $leaveReport_data ['count'];
					$this->setListComponent ( $leaveReport_data ['data'], $count, $pageNumber, $noOfRecords, $reportType, $reportType );
				}
			}
		}
	}
	/**
	 * export excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Engineer Resource Utilization Report TCI.xls
	 */
	public function exportExcel_leaveReport(ResourceReportSearchParameters $srchForExport, $Export_data) {
		$object = new exportExcel ();
		$object->setTypeReport ( 4 );
		$this->checkType = $object->getTypeReport ();
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3 || $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $Export_data ['count'];
			if ($this->checkCount != 0) {
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $srchForExport->getFromDate () . ' to ' . $srchForExport->getToDate () );
				$object->setNameReport ( 'Leave Report' );
				$object->setFileName ( 'Leave Report' );
				$data = array ();
				$title = array (
						"Employee's Name",
						'Date',
						'Leave Type',
						"Number of Date" 
				);
				foreach ( $Export_data ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [0],
							$title [1] => $a [1],
							$title [2] => $a [2],
							$title [3] => $a [3] 
					) );
				}
				$object->createReport ( $title, $data );
			}
		}
	}
}