<?php
/**
 *
 * @author Vo Tien Thieu
 * @name : ResourceReportAction
 *       2015-07-24
 *       Action for Report
 */
class ResourceReportAction extends sfAction {
		
	/**
	 * @author Vo Tien Thieu
	 * @param $request
	 */
	public function execute($request) {
            $defaultPath = 'resourceReport/TimesheetDetails';
            $this->redirect($defaultPath);
	}
}

?>