<?php
/**
 * @author: Le Minh Duc
 * @name : ProjectresourceUtilizationAction
 * @date : 2015-07-22
 * Action for report Project Resource Utilization
 * Last Modified: 15:10, 11/08/2015, by Luu Quoc Bao
 * Reason: extend base resource report class to reduce code
 */
class ProjectResourceUtilizationAction extends BaseResourceReportAction {
	/**
	 * execute report Project Resource Utilization 
	 * Desription :
	 * Create a form to get user input
	 * Call Query from resourceReportService to get data for report Project Resource Ultilization 
	 * Query's result keep in $ProjectResourceUtilization_Data when catch method post
	 * Create a list to show $ProjectResourceUtilization_Data.
	 *
	 * @author Nguyen Thanh Dat
	 * @param
	 *        	$request
	 * @return Call file ProjectResourceUtilizationSuccess
	 */
	public function execute($request) {
		$searchParam = new ResourceReportSearchParameters ();
		$searchParam->setSortField ( 'NameOfProject' );
		$sortField = $request->getParameter ( 'sortField' );
		$sortOrder = $request->getParameter ( 'sortOrder' );
		$isPaging = $request->getParameter ( 'pageNo' );
		$pageNumber = $isPaging;
		if (! is_null ( $this->getUser ()->getAttribute ( 'PRU_PageNumber' ) ) && ! ($pageNumber >= 1)) {
			$pageNumber = $this->getUser ()->getAttribute ( 'PRU_PageNumber' );
		}
		$this->getUser ()->setAttribute ( 'PRU_PageNumber', $pageNumber );
		// / set for test
		$searchParam->setLimit ( sfConfig::get ( 'app_items_per_page' ) );
		$noOfRecords = $searchParam->getLimit ();
		$offset = ($pageNumber >= 1) ? (($pageNumber - 1) * $noOfRecords) : ($request->getParameter ( 'pageNo', 1 ) - 1) * $noOfRecords;
		$this->setForm ( new ProjectResourceUtilization_Form () ); // Create Form to get User Input
		if (! empty ( $sortField ) && ! empty ( $sortOrder ) || $isPaging > 0) {
			if ($this->getUser ()->hasAttribute ( 'PRU_SearchParameters' )) {
				$searchParam = $this->getUser ()->getAttribute ( 'PRU_SearchParameters' );
				$this->form->setDefaultDataToWidgets ( $searchParam );
			}
			$searchParam->setSortField ( $sortField );
			$searchParam->setSortOrder ( $sortOrder );
		} else {
			$this->getUser ()->setAttribute ( 'PRU_SearchParameters', $searchParam );
			$offset = 0;
			$pageNumber = 1;
		}
		$searchParam->setOffset ( $offset );
		$reportType = 'PRU';
		$ProjectResourceUtilization_Data = $this->getResourceReportService ()->getProjectResourceUtilization_Data ( $searchParam );
		$this->setListComponent ( $ProjectResourceUtilization_Data ['data'], $ProjectResourceUtilization_Data ['count'], $pageNumber, $noOfRecords, $reportType );
		// ////////////////////////////////////////////////////////////////////////
		// Catch method post from client to call query and show result
		if (empty ( $isPaging )) {
			if ($request->isMethod ( 'post' )) {
				$pageNumber = 1;
				$searchParam->setOffset ( 0 );
				$this->getUser ()->setAttribute ( 'PRU_PageNumber', $pageNumber );
				
				$this->form->bind ( $request->getParameter ( $this->form->getName () ) );
				$searchParam = $this->form->getResourceReportSearchParams ( $searchParam );
				
				$this->getUser ()->setAttribute ( 'PRU_SearchParameters', $searchParam );
				if ($this->form->isValid ()) {
					
					if ($_POST ['exportExcel'] == 1) {
						// get all data for export
						$srchForExport = new ResourceReportSearchParameters ();
						$srchForExport = $this->form->getResourceReportSearchParams ( $srchForExport );
						$srchForExport->setOffset ( 0 );
						$srchForExport->setSortField ( 'NameOfProject' );
						$srchForExport->setLimit ( 0 );
						$exportData = $this->getResourceReportService ()->getProjectResourceUtilization_Data ( $srchForExport );
						$this->exportExcel_PRU ( $srchForExport, $exportData );
						return;
					}
					$ProjectResourceUtilization_Data = $this->getResourceReportService ()->getProjectResourceUtilization_Data ( $searchParam );
					$count = $ProjectResourceUtilization_Data ['count'];
					$this->setListComponent ( $ProjectResourceUtilization_Data ['data'], $count, $pageNumber, $noOfRecords, $reportType );
				}
			}
		}
	}
	/**
	 * export excel
	 *
	 * @author Nguyen Minh Tuan
	 * @param        	
	 *
	 * @return Project Resource Utilization Report.xls
	 */
	public function exportExcel_PRU($srchForExport, $exportData) {
		$object = new exportExcel ();
		$object->setTypeReport ( 6 );
		$this->checkType = $object->getTypeReport ();
		// check type report
		if ($this->checkType == 1 || $this->checkType == 2 || $this->checkType == 3 || $this->checkType == 4 || $this->checkType == 5 || $this->checkType == 6) {
			$this->checkCount = $exportData ['count'];
			if ($this->checkCount != 0) {
				$data = array ();
				$title = array (
						'Bill',
						"Name of project",
						'Request By',
						'Target',
						"Actual (%)",
						'Actual Hours',
						'Project head count',
						'Utilization Margin',
						'Head count' 
				);
				foreach ( $exportData ['data'] as $i => $a ) {
					array_push ( $data, array (
							$title [0] => $a [0],
							$title [1] => $a [1],
							$title [2] => $a [2],
							$title [3] => $a [3],
							$title [4] => $a [4],
							$title [5] => $a [5],
							$title [6] => $a [6],
							$title [7] => $a [7] 
					) );
				}
				
				$object->setNameReportBy ( $this->getUser ()->getAttribute ( 'auth.firstName' ) );
				$object->setNameReportTo ( '' );
				$object->setTimeLineReport ( $srchForExport->getFromDate () . ' to ' . $srchForExport->getToDate () );
				$object->setNameReport ( 'Project Resource Utilization' );
				$object->setFileName ( 'Project Resource Utilization Report' );
				$object->createReport ( $title, $data );
			}
		}
	}
}
?>