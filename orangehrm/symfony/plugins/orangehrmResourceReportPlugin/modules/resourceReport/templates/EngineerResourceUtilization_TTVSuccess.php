<style>
.searchForm form ol li input.calendar { width: 75% ;}
</style>
<?php
 /**
 * 
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_TTVSuccess
 * 2015-07-22
 * @description :
 * Call by EngineerResourceUtilization_TTVAction to show result
 */
include 'exportExcel.php';
use_stylesheets_for_form($form);
use_javascripts_for_form($form);
use_javascript(plugin_web_path('orangehrmResourceReportPlugin', 'js/EngineerResourceUtilization_Success')); 
?>
<script type="text/javascript" language="javascript">
var datepickerDateFormat = '<?php echo get_datepicker_date_format($sf_user->getDateFormat()); ?>';
var lang_dateError = '<?php echo __("To date should be after from date") ?>';
var lang_validDateMsg = '<?php echo __(ValidationMessages::DATE_FORMAT_INVALID, array('%format%' => str_replace('yy', 'yyyy', get_datepicker_date_format($sf_user->getDateFormat())))) ?>';
var lang_required = '<?php echo __(ValidationMessages::REQUIRED); ?>';
var lang_nameError = '<?php echo __("Please select a valid name") ?>';
var lang_limitedDate = '<?php echo __("Can not select future date")?>';
var employee = <?php echo str_replace('&#039;', "'", $form->getEmployeeListAsJson()) ?> ;
var employeeArray = eval(employee);

function myReload(){
    document.getElementById("checkdata").style.display = "none";
    document.getElementById("checktype").style.display = "none";
}
</script>


<div class="box searchForm toggableForm" id="srchEngineerResourceUtilization">
    <div class="head">
        <h1><?php echo __('Engineer Resource Utilization TTV'); ?></h1>
    </div>
    <div class="inner">
        
        <p id="checkdata" style="color: brown; font-style= italic;" >
                        <?php
                        if($checkCount != 0 || $checkCount==array())
                            echo '';
                        else if ( $_POST ['exportExcel'] == 1){
                            echo 'No Records Found';
                        }
                        ?>
          </p>
          <p id="checktype" style="color: brown; font-style= italic; margin-bottom : 10px;" >
          				
                        <?php
                        if($checkType != 1 && $checkType != 2 && $checkType != 3 && $checkType != 4 && $checkType != 5 && $checkType != 6 && $_POST ['exportExcel'] == 1  )
                            echo 'Error Type Report';
                        ?>
         </p>      
        <form name="frmSrchEngineerResourceUtilization" id="frmSrchEngineerResourceUtilization" method="post" action="<?php echo url_for('resourceReport/EngineerResourceUtilization_TTV'); ?>">
            <fieldset>
              
                <ol>
                    <?php echo $form->render(); ?>
                    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'listing_layout_navigation_bar_1')); ?>
                </ol>
                            
                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />
                    <input type="button" id="btnRst" class="reset" value="<?php echo __("Reset") ?>" name="btnRst"/>
                    <input type="hidden"  value="" name="exportExcel"  id="exportExcel"/>
					<input type="button" class="reset" id="report" value="<?php echo __("Export") ?>" name="btnSrch"   /> 
                </p>
                
            </fieldset>
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>

<?php include_component('core', 'ohrmList'); ?>

<?php 
/**
 * @author: Vo Tin Thieu
 * submit URL and pageNo
 */
?>
<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('resourceReport/EngineerResourceUtilization_TTV'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>


<script type="text/javascript">
	<?php 
	/**
	 * @author: Vo Tin Thieu
	 *  submit pageNo for form
	 */
	?>
    function submitPage(pageNo) {
        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
</script>



