<style>
.searchForm form ol li input.calendar { width: 75% ;}
</style>
<?php 
/**
 *
 * @author Nguyen Minh Cuong
 * @name:
 * 2015-07-22
 * Form for report Project Resource Utilization - TTV
 */
use_stylesheets_for_form($form);
use_javascripts_for_form($form);
use_javascript(plugin_web_path('orangehrmResourceReportPlugin', 'js/ProjectResourceUtilization_Success')); 
?>

<script type="text/javascript" language="javascript">
var datepickerDateFormat = '<?php echo get_datepicker_date_format($sf_user->getDateFormat()); ?>';
var lang_dateError = '<?php echo __("To date should be after from date") ?>';
var lang_validDateMsg = '<?php echo __(ValidationMessages::DATE_FORMAT_INVALID, array('%format%' => str_replace('yy', 'yyyy', get_datepicker_date_format($sf_user->getDateFormat())))) ?>';
var lang_required = '<?php echo __(ValidationMessages::REQUIRED); ?>';

function myReload(){
    document.getElementById("checkdata").style.display = "none";
    document.getElementById("checktype").style.display = "none";
}
</script>
<div class="box searchForm toggableForm" id="srchProjectResourceUtilization">
    <div class="head">
        <h1><?php echo __('Project Resource Utilization TTV'); ?></h1>
    </div>

    <div class="inner">
    
    	<p id="checkdata" style="color: brown; font-style= italic;" >
                        <?php
                    
                        if($checkCount != 0 || $checkCount==array())
                            echo '';
                        else if ( $_POST ['exportExcel'] == 1){
                            echo 'No Records Found';
                        }
                        ?>
         </p>
         <p id="checktype" style="color: brown; font-style= italic; margin-bottom : 10px;" >
          				
                        <?php
                        if($checkType != 1 && $checkType != 2 && $checkType != 3 && $checkType != 4 && $checkType != 5 && $checkType != 6 && $_POST ['exportExcel'] == 1  )
                            echo 'Error Type Report';
                        ?>
         </p>

        <form name="frmSrchProjectResourceUtilization" id="frmSrchProjectResourceUtilization" method="post" action="<?php echo url_for('resourceReport/ProjectResourceUtilization_TTV'); ?>">
            
            <fieldset>
              
                <ol>
                    <?php echo $form->render(); ?>
                    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'listing_layout_navigation_bar_1')); ?>
                </ol>
                            
                <p>
                    <input type="button" id="btnSrch" value="<?php echo __("Search") ?>" name="btnSrch" />
                    <input type="button" id="btnRst" class="reset" value="<?php echo __("Reset") ?>" name="btnRst" />  
                    <input type="hidden" id="exportExcel" value="" name="exportExcel" />
					<input type="button" class="reset" id="report" value="<?php echo __("Export") ?>" name="btnSrch"   />  
                                         
                </p>
            </fieldset>
        </form>
    </div>
    <a href="#" class="toggle tiptip" title="<?php echo __(CommonMessages::TOGGABLE_DEFAULT_MESSAGE); ?>">&gt;</a>
</div>

<?php include_component('core', 'ohrmList'); ?>


<?php 
/**
 * @author: orangehrm
 * submit URL and pageNo
 */
?>
<form name="frmHiddenParam" id="frmHiddenParam" method="post" action="<?php echo url_for('resourceReport/ProjectResourceUtilization_TTV'); ?>">
    <input type="hidden" name="pageNo" id="pageNo" />
    <input type="hidden" name="hdnAction" id="hdnAction" value="search" />
</form>


<script type="text/javascript">
	<?php 
	/**
	 * @author: orangehrm
	 *  submit pageNo for form
	 */
	?>
    function submitPage(pageNo) {
        document.frmHiddenParam.pageNo.value = pageNo;
        document.frmHiddenParam.hdnAction.value = 'paging';
        document.getElementById('frmHiddenParam').submit();

    }
</script>
