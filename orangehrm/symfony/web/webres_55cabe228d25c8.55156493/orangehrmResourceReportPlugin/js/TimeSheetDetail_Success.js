 /**
 * 
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_Success.js 
 * 2015-07-22
 * @description :
 * validate user's input
 */
$(document).ready(function() { 
    $('#btnSrch').click(function() {
        $('#frmSrchEngineerResourceUtilization').submit();
   
    });
    $('#btnRst').click(function() {
        location.replace(location);
    });
    
    $('#report').click(function() {
        $('#btnSrch').load(myReload());
        $('#exportExcel').val(1);
        $('#frmSrchEngineerResourceUtilization').submit();
        
        $('#exportExcel').val('');
    });
	
	$('#resourceReport_employeeName_empName').click(function(){
		if($(this).val() === 'Type for hints...')
			$(this).val('');
	});
    
    $('#resourceReport_employeeName_empName').result(function(event, item) {
        $(this).valid();
    });
    
    var now =  $.datepicker.formatDate(datepickerDateFormat, new Date());
    var validator = $("#frmSrchEngineerResourceUtilization").validate({

        rules: {
            'resourceReport[reportFromDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                },
                date_range_limit: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        toDate:now
                    }
                }
               
            },
            'resourceReport[reportToDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                },
                date_range: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        fromDate:$('#report_fromDate').val(),
                    }
                },
                date_range_limit: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        toDate:now
                    }
                }
            },
            'resourceReport[employeeName][empName]':{
        		empNameValidation: true,
               // onkeyup: false
            }
        },
        messages: {
            'resourceReport[reportFromDate]' : {
                valid_date: lang_validDateMsg,
                date_range_limit: lang_limitedDate
            },
            'resourceReport[reportToDate]' : {               
                valid_date: lang_validDateMsg,
                date_range: lang_dateError,
                date_range_limit: lang_limitedDate
            },
            'resourceReport[employeeName][empName]':{
                empNameValidation: lang_nameError
            }
        },          
    });
    
    $.validator.addMethod('date_range_limit', function(value, element, params) {

        var valid = false;

        if (params.fromDate != undefined) {
            var fromDate = $.trim(params.fromDate);
            var toDate = $.trim(value);
        } else {
            var fromDate = $.trim(value); 
            var toDate = $.trim(params.toDate);       
        }
        var format = params.format;
        var displayFormat = '';

        if (params.displayFormat != undefined) {
            displayFormat = params.displayFormat;
        } else {
            displayFormat = format;
        }    

        if(fromDate == displayFormat || toDate == displayFormat || fromDate == "" || toDate =="") {
            valid = true;
        }else{
            var parsedFromDate = $.datepicker.parseDate(format, fromDate);
            var parsedToDate = $.datepicker.parseDate(format, toDate);
            if(parsedFromDate <= parsedToDate){
                valid = true;
            }
        }
        return valid;
    });
    
    $.validator.addMethod("empNameValidation", function(value, element, params) {
        var valid = false;
        var empCount = employeeArray.length;
        if ($('#resourceReport_employeeName_empName').val() === "Type for hints...") {
        	$('#resourceReport_employeeName_empId').val("");
        	valid = true;
        }

        else if ($('#resourceReport_employeeName_empName').val() == "") {
        	$('#resourceReport_employeeName_empId').val("");
        	valid = true;
        }
        else{
            var i;
            for (i=0; i < empCount; i++) {
                empName = $.trim($('#resourceReport_employeeName_empName').val());
                arrayName = employeeArray[i].name;
                if (empName == arrayName) {
                	valid = true
                    break;
                }
            }
        }
        return valid;
    });
    var Type = 'odd';
     $('#resultTable tbody tr').each(function() {
        $(this).removeClass().addClass(Type);
        $(this).find('td').each(function() {
            if ( ($(this).text() == "Annual Leave") || ($(this).text() == "Leave") ) {
                $(this).parent().css("border-bottom","1px solid #989898");
                (Type=="odd")? Type="even" : Type="odd";
                return Type;
            }
        });
    });

    $('#frmList_ohrmListComponent').attr('name','frmList_ohrmListComponent');

});
