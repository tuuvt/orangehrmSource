 /**
 * 
 * @author Luu Quoc Bao
 * @name: EngineerResourceUtilization_Success.js 
 * 2015-07-28
 * @description :
 * validate user's input
 */
$(document).ready(function() {
    $('#btnSrch').click(function() {
        $('#frmSrchProjectResourceUtilization').submit();
   
    });
    $('#btnRst').click(function() {
    	location.replace(location);
    });
    
    $('#report').click(function() {
        $('#btnSrch').load(myReload());
        $('#exportExcel').val(1);
        $('#frmSrchProjectResourceUtilization').submit();
        $('#exportExcel').val('');
    });

    
    var validator = $("#frmSrchProjectResourceUtilization").validate({

        rules: {
            'resourceReport[reportFromDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                },
                date_range_limit: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        toDate:now
                    }
                }
            },
            'resourceReport[reportToDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:true,
                        displayFormat:displayDateFormat
                    }
                },
                date_range: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        fromDate:$('#report_fromDate').val()
                    }
                },
                date_range_limit: function() {
                    return {
                        format:datepickerDateFormat,
                        displayFormat:displayDateFormat,
                        toDate:now
                    }
                }
            }
        },
        messages: {
            'resourceReport[reportFromDate]' : {
                valid_date: lang_validDateMsg,
                date_range_limit: lang_limitedDate
            },
            'resourceReport[reportToDate]' : {
                valid_date: lang_validDateMsg,
                date_range: lang_dateError,
                date_range_limit: lang_limitedDate
            }
        },          
    });
    
    var now =  $.datepicker.formatDate(datepickerDateFormat, new Date());
    $.validator.addMethod('date_range_limit', function(value, element, params) {

        var valid = false;

        if (params.fromDate != undefined) {
            var fromDate = $.trim(params.fromDate);
            var toDate = $.trim(value);
        } else {
            var fromDate = $.trim(value); 
            var toDate = $.trim(params.toDate);       
        }
        var format = params.format;
        var displayFormat = '';

        if (params.displayFormat != undefined) {
            displayFormat = params.displayFormat;
        } else {
            displayFormat = format;
        }    

        if(fromDate == displayFormat || toDate == displayFormat || fromDate == "" || toDate =="") {
            valid = true;
        }else{
            var parsedFromDate = $.datepicker.parseDate(format, fromDate);
            var parsedToDate = $.datepicker.parseDate(format, toDate);
            if(parsedFromDate <= parsedToDate){
                valid = true;
            }
        }
        return valid;
    });

    $('#frmList_ohrmListComponent').attr('name','frmList_ohrmListComponent');

});
