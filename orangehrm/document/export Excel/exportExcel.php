<?php

/*
 * @ name : Nguyen Minh Tuan
 * @ To do list :
 */
// constan for set horizontal
define ( HORIZONTAL_CENTER, 'center' );
define ( HORIZONTAL_LEFT, 'left' );
define ( HORIZONTAL_RIGHT, 'right' );
class exportExcel {
	private $arr = array (
			
			'1' => array (
					'width' => array (
							10,
							35,
							15,
							15,
							15,
							18,
							35,
							30 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							7,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'B2',
							'report_by_val' => 'C2',
							'report_to' => 'B3',
							'report_to_val' => 'C3',
							'time_line' => 'G2',
							'time_line_val' => 'H2',
							'date' => 'G3',
							'date_val' => 'H3' 
					),
					'boder' => array (
							'A',
							2,
							8,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'C',
									4 
							),
							array (
									'left',
									'B',
									1 
							),
							array (
									'left',
									'G',
									2 
							) 
					),
					'print_margin' => array (
							'left' => 0.4,
							'right' => 0.7 
					) 
			),
			'2' => array (
					'width' => array (
							10,
							35,
							20,
							20,
							15,
							15,
							18,
							18,
							18 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							8,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'B2',
							'report_by_val' => 'C2',
							'report_to' => 'B3',
							'report_to_val' => 'C3',
							'time_line' => 'G2',
							'time_line_val' => 'H2',
							'date' => 'G3',
							'date_val' => 'H3' 
					),
					'boder' => array (
							'A',
							2,
							9,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'D',
									6 
							),
							array (
									'left',
									'B',
									2 
							) 
					),
					'print_margin' => array (
							'left' => 0.35,
							'right' => 0.7 
					) 
			),
			'3' => array (
					'width' => array (
							10,
							10,
							35,
							25,
							35,
							30,
							15,
							12 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							7,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'C2',
							'report_by_val' => 'D2',
							'report_to' => 'C3',
							'report_to_val' => 'D3',
							'time_line' => 'F2',
							'time_line_val' => 'G2',
							'date' => 'F3',
							'date_val' => 'G3' 
					),
					'boder' => array (
							'A',
							2,
							8,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'B',
									1 
							),
							array (
									'center',
									'G',
									2 
							),
							array (
									'left',
									'C',
									4 
							) 
					),
					'print_margin' => array (
							'left' => 0.45,
							'right' => 0.7 
					) 
			),
			'4' => array (
					'width' => array (
							10,
							40,
							20,
							22,
							25 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							4,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'B2',
							'report_by_val' => 'C2',
							'report_to' => 'B3',
							'report_to_val' => 'C3',
							'time_line' => 'D2',
							'time_line_val' => 'E2',
							'date' => 'D3',
							'date_val' => 'E3' 
					),
					'boder' => array (
							'A',
							2,
							5,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'E',
									1 
							),
							array (
									'left',
									'D',
									1 
							),
							array (
									'center',
									'C',
									1 
							) 
					),
					'print_margin' => array (
							'left' => 0.8,
							'right' => 0.7 
					) 
			),
			'5' => array (
					'width' => array (
							10,
							35,
							15,
							15,
							15,
							15,
							18,
							35,
							25 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							8,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'C2',
							'report_by_val' => 'D2',
							'report_to' => 'C3',
							'report_to_val' => 'D3',
							'time_line' => 'H2',
							'time_line_val' => 'I2',
							'date' => 'H3',
							'date_val' => 'I3' 
					),
					'boder' => array (
							'A',
							2,
							9,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'B',
									1 
							),
							array (
									'left',
									'C',
									1 
							),
							array (
									'center',
									'D',
									4 
							),
							array (
									'left',
									'H',
									2 
							) 
					),
					'print_margin' => array (
							'left' => 0.4,
							'right' => 0.7 
					) 
			),
			'6' => array (
					'width' => array (
							10,
							10,
							35,
							20,
							18,
							15,
							15,
							18,
							18,
							16 
					),
					'logo' => array (
							'height' => 250,
							'width' => 1200 
					),
					'merge' => array (
							'B',
							1,
							9,
							1 
					),
					'title' => array (
							'name_report' => 'B1',
							'report_by' => 'C2',
							'report_by_val' => 'D2',
							'report_to' => 'C3',
							'report_to_val' => 'D3',
							'time_line' => 'H2',
							'time_line_val' => 'I2',
							'date' => 'H3',
							'date_val' => 'I3' 
					),
					'boder' => array (
							'A',
							2,
							10,
							5 
					),
					'horizontal' => array (
							array (
									'center',
									'B',
									1 
							),
							array (
									'left',
									'C',
									2 
							),
							array (
									'center',
									'E',
									6 
							) 
					),
					'print_margin' => array (
							'left' => 0.7,
							'right' => 0.7 
					) 
			) 
	);
	private $typeReport;
	private $nameReportBy;
	private $nameReportTo;
	private $timeLineReport;
	private $nameReport;
	private $filename;
	private $path = "./logo.png";
	private $style = array (
			'font' => array (
					'bold' => true,
					'color' => array (
							'rgb' => '111111' 
					),
					'size' => 10,
					'name' => 'Arial' 
			),
			'alignment' => array (
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
			) 
	);
	private $styleBoderHeder = array (
			'borders' => array (
					'allborders' => array (
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array (
									'rgb' => 'FFFFFF' 
							) 
					) 
			) 
	);
	private $styleTable = array (
			'borders' => array (
					'allborders' => array (
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array (
									'rgb' => '111111' 
							) 
					) 
			) 
	);
	private $nameHeader = array (
			'Report By: ',
			'Report To: ',
			'Report Timeline: ',
			'Report Date: ' 
	);
	private $styleNameReport = array (
			'font' => array (
					'bold' => true,
					'color' => array (
							'rgb' => '111111' 
					),
					'size' => 16,
					'name' => 'Arial' 
			),
			'alignment' => array (
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
			) 
	);
	private $styleReportRight = array (
			'font' => array (
					'bold' => true,
					'color' => array (
							'rgb' => '111111' 
					),
					'size' => 11,
					'name' => 'Arial' 
			),
			'alignment' => array (
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
			) 
	);
	private $styleReportLeft = array (
			'font' => array (
					'bold' => false,
					'color' => array (
							'rgb' => '111111' 
					),
					'size' => 11,
					'name' => 'Arial' 
			),
			'alignment' => array (
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
			) 
	);
	
	/**
	 *
	 * @author team export
	 * @name setNameReportBy
	 * @todo Set nameReportBy
	 * @param $nameReportBy: string        	
	 */
	public function setNameReportBy($nameReportBy) {
		$this->nameReportBy = $nameReportBy;
	}
	/**
	 *
	 * @author team export
	 * @name setNameReportTo
	 * @todo Set nameReportTo
	 * @param $nameReportTo: string        	
	 */
	public function setNameReportTo($nameReportTo) {
		$this->nameReportTo = $nameReportTo;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setTimeLineReport
	 * @todo Set timeLineReport
	 * @param $timeLineReport: string        	
	 */
	public function setTimeLineReport($timeLineReport) {
		$this->timeLineReport = $timeLineReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setNameReport
	 * @todo Set nameReport
	 * @param $nameReport: string        	
	 */
	public function setNameReport($nameReport) {
		$this->nameReport = $nameReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setTypeReport
	 * @todo Set typeReport
	 * @param $typeReport: string        	
	 */
	public function setTypeReport($typeReport) {
		$this->typeReport = $typeReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setFileName
	 * @todo Set filename
	 * @param $filename: string        	
	 */
	public function setFileName($filename) {
		$this->filename = $filename;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getFileName
	 * @todo get filename
	 * @return filename
	 */
	public function getFileName() {
		return $this->filename;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getNameReportBy
	 * @todo get nameReportBy
	 * @return nameReportBy
	 */
	public function getNameReportBy() {
		return $this->nameReportBy;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getNameReportTo
	 * @todo get nameReportTo
	 * @return nameReportTo
	 */
	public function getNameReportTo() {
		return $this->nameReportTo;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getTimeLineReport
	 * @todo get timeLineReport
	 * @return timeLineReport
	 */
	public function getTimeLineReport() {
		return $this->timeLineReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getNameReport
	 * @todo get nameReport
	 * @return nameReport
	 */
	public function getNameReport() {
		return $this->nameReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name getTypeReport
	 * @todo get typeReport
	 * @return typeReport
	 */
	public function getTypeReport() {
		return $this->typeReport;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name header
	 * @todo Create Header for File report
	 */
	private function header(PHPExcel $objPHPExcel) {
		$this->setLogo ( $objPHPExcel, $this->arr [$this->typeReport] ['logo'] ['width'], $this->arr [$this->typeReport] ['logo'] ['height'], $this->path );
		$this->setHeight ( $objPHPExcel, 1, 70 );
		$this->setMerge ( $objPHPExcel, $this->arr [$this->typeReport] ['merge'] [0], $this->arr [$this->typeReport] ['merge'] [1], $this->arr [$this->typeReport] ['merge'] [2], $this->arr [$this->typeReport] ['merge'] [3] );
		$count = 1;
		for($i = 0; $i < count ( $this->arr [$this->typeReport] ['width'] ); $i ++) {
			$this->setWidth ( $objPHPExcel, $this->numToStr ( $count ), $this->arr [$this->typeReport] ['width'] [$i] );
			$count ++;
		}
		$this->boderHeder ( $objPHPExcel, $this->styleBoderHeder, $this->arr [$this->typeReport] ['boder'] [0], $this->arr [$this->typeReport] ['boder'] [1], $this->arr [$this->typeReport] ['boder'] [2], $this->arr [$this->typeReport] ['boder'] [3] );
		$this->setText1 ( $objPHPExcel, $this->styleNameReport, $this->arr [$this->typeReport] ['title'] ['name_report'], strtoupper ( $this->nameReport ) );
		$this->setText1 ( $objPHPExcel, $this->styleReportRight, $this->arr [$this->typeReport] ['title'] ['report_by'], $this->nameHeader [0] );
		$this->setText1 ( $objPHPExcel, $this->styleReportRight, $this->arr [$this->typeReport] ['title'] ['report_to'], $this->nameHeader [1] );
		$this->setText1 ( $objPHPExcel, $this->styleReportLeft, $this->arr [$this->typeReport] ['title'] ['report_by_val'], $this->nameReportBy );
		$this->setText1 ( $objPHPExcel, $this->styleReportLeft, $this->arr [$this->typeReport] ['title'] ['report_to_val'], $this->nameReportTo );
		$this->setText1 ( $objPHPExcel, $this->styleReportRight, $this->arr [$this->typeReport] ['title'] ['time_line'], $this->nameHeader [2] );
		$this->setText1 ( $objPHPExcel, $this->styleReportRight, $this->arr [$this->typeReport] ['title'] ['date'], $this->nameHeader [3] );
		$date1 = substr ( $this->getTimeLineReport (), 0, 10 );
		$date2 = substr ( $this->getTimeLineReport (), 0, 10 );
		$startDate = substr ( $this->getTimeLineReport (), 8, 2 ) . '/' . substr ( $this->getTimeLineReport (), 5, 2 ) . '/' . substr ( $this->getTimeLineReport (), 0, 4 );
		$endDate = substr ( $this->getTimeLineReport (), 22, 2 ) . '/' . substr ( $this->getTimeLineReport (), 19, 2 ) . '/' . substr ( $this->getTimeLineReport (), 14, 4 );
		$this->setText1 ( $objPHPExcel, $this->styleReportLeft, $this->arr [$this->typeReport] ['title'] ['time_line_val'], $startDate . substr ( $this->getTimeLineReport (), 10, 4 ) . $endDate );
		$this->setText1 ( $objPHPExcel, $this->styleReportLeft, $this->arr [$this->typeReport] ['title'] ['date_val'], '' );
		$date = new DateTime ();
		$objPHPExcel->getActiveSheet ()->setCellValue ( $this->arr [$this->typeReport] ['title'] ['date_val'],   $date->format('d/m/Y')  );
	
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name createReport
	 * @todo create file Report
	 * @param $title:array ,
	 *        	$data: array
	 */
	public function createReport($title, $data) {
		$objPHPExcel = new PHPExcel ();
		$this->header ( $objPHPExcel );
		if ($this->typeReport == 5) {
			$this->createTableERUD( $objPHPExcel, $title, $data );
		} else if ($this->typeReport == 6) {
			$this->createTablePRU( $objPHPExcel, $title, $data );
		} else {
			$this->createTable ( $objPHPExcel, $title, $data );
		}
		$objPHPExcel->getActiveSheet ()->getPageSetup ()->setPaperSize ( PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4 );
		$objPHPExcel->getActiveSheet ()->getPageSetup ()->setFitToPage ( true );
		$objPHPExcel->getActiveSheet ()->getPageSetup ()->setOrientation ( PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE )->setFitToWidth ( 1 )->setFitToHeight ( 0 );
		$objPHPExcel->getActiveSheet ()->getPageMargins ()->setLeft ( $this->arr [$this->typeReport] ['print_margin'] ['left'] );
		$objPHPExcel->getActiveSheet ()->getPageMargins ()->setRight ( $this->arr [$this->typeReport] ['print_margin'] ['right'] );
		
		header ( 'Content-Disposition: attachment;filename="' . $this->getFileName () . '.xls' . '"' );
		header ( 'Cache-Control: max-age=0' );
		$objWriter = PHPExcel_IOFactory::createWriter ( $objPHPExcel, 'Excel5' );
		$objWriter->save ( 'php://output' );
	}
	
	// -----------------FUNCTION---------------------------
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name createTable
	 * @todo create Table for report EngineerResourceUtilization_TTV, ProjectResourceUtilization_TTV, TimeSheet_Details, LeaveReport
	 * @param $objPHPExcel:PHPExcel, $title:array
	 *        	, $data:array
	 */
	private function createTable(PHPExcel $objPHPExcel, $title, $data) {
		$width = count ( $title );
		$height = count ( $data );
		$startCol = 'B';
		$startColumID = 2;
		$startRowID = 7;
		if ($this->typeReport == 2) {
			$nextCol = 'G';
			$color = 'E0E0E0';
			$this->boderHeder ( $objPHPExcel, $this->styleTable, $nextCol, 6, 3, 1 );
			$this->cellColor ( $objPHPExcel, $color, $nextCol, 6, 3, 1 );
			$this->cellColor ( $objPHPExcel, $color, $startCol, 7, $width, 1 );
			$this->setText1 ( $objPHPExcel, $this->style, 'G6', 'Next Target' );
			$this->setMerge ( $objPHPExcel, 'G', 6, 3, 1 );
			$objPHPExcel->getActiveSheet ()->getStyle ( 'B7:I7' )->getAlignment ()->setWrapText ( true );
		}
		if ($this->typeReport == 3) {
			$startColumID ++;
			$width ++;
		}
		$this->boderHeder ( $objPHPExcel, $this->styleTable, $startCol, 7, $width, 1 );
		$countCol = $startColumID;
		for($i = 0; $i < $width; $i ++) {
			$var = $this->numToStr ( $countCol );
			$this->setText1 ( $objPHPExcel, $this->style, $var . $startRowID, $title [$countCol - $startColumID] );
			$countCol ++;
		}
		
		if ($this->typeReport == 1)
			$this->setAutofilter ( $objPHPExcel, 'B', 7, 7, 1 );
		
		$countRow = 8;
		
		if ($this->typeReport == 3) {
			$temp = $data [0] [$title [0]];
			$count = 1;
			$countID = 0;
		} else if ($this->typeReport == 4) {
			$name = $data [0] [$title [0]];
			$type = $data [0] [$title [2]];
			$count1 = 0;
			$sum = 0;
		}
		if ($this->typeReport == 4) {
			$this->boderHeder ( $objPHPExcel, $this->styleTable, 'B', 8, $width, $height );
			$this->setText ( $objPHPExcel, 'Arial', 10, false, $this->numToStr ( $startColumID ), $startRowID + 1, $width, $height );
		} else {
			$this->boderHeder ( $objPHPExcel, $this->styleTable, 'B', 8, $width, $height );
			$this->setText ( $objPHPExcel, 'Arial', 10, false, $this->numToStr ( $startColumID ), $startRowID + 1, $width, $height );
		}
		foreach ( $data as $row ) {
			if ($this->typeReport == 4) {
				$countCol = 2;
				for($i = 0; $i < $width; $i ++) {
					if ($row [$title [2]] == 'Total') {
						$this->setMerge ( $objPHPExcel, 'B', $countRow, 3, 1 );
						$objPHPExcel->setActiveSheetIndex ()->setCellValue ( 'B' . $countRow, 'Total' );
						$this->setHorizontal ( $objPHPExcel, HORIZONTAL_CENTER, 'B', $countRow, 1, 1 );
						$objPHPExcel->getActiveSheet ()->getStyle ( 'E' . $countRow )->getFont ()->setBold ( true );
						$this->cellColor ( $objPHPExcel, 'FFFF80', 'E', $countRow, 1, 1 );
					}
					$var = $this->numToStr ( $countCol );
					if ($i == 1) {
						$objPHPExcel->setActiveSheetIndex ()->setCellValue ( $var . $countRow, $row [$title [$countCol - $startColumID]] );
					} else {
						$objPHPExcel->setActiveSheetIndex ()->setCellValue ( $var . $countRow, $row [$title [$countCol - $startColumID]] );
					}
					$countCol ++;
				}
				$countRow ++;
			} else {
				$countCol = 2;
				if ($this->typeReport == 3) {
					if ($row [$title [0]] == '') {
						$this->setMerge ( $objPHPExcel, 'B', $countRow - $count + 1, 1, $count );
						$countID ++;
						$objPHPExcel->setActiveSheetIndex ()->setCellValue ( 'B' . ($countRow - $count + 1), $countID );
						$this->setText ( $objPHPExcel, 'Arial', 10, false, 'B', ($countRow - $count + 1), 1, 1 );
						$count = 0;
					}
					$count ++;
				}
				for($i = 0; $i < $width; $i ++) {
					$var = $this->numToStr ( $countCol );
					$objPHPExcel->setActiveSheetIndex ()->setCellValue ( $var . $countRow, $row [$title [$countCol - $startColumID]] );
					$countCol ++;
				}
				$countRow ++;
			}
		}
		if ($this->typeReport == 3) {
			if ($height > 0) {
				$countID ++;
				$this->setText ( $objPHPExcel, 'Arial', 10, false, 'B', ($countRow - $count), 1, 1 );
			}
		}
		for($i = 0; $i < count ( $this->arr [$this->typeReport] ['horizontal'] ); $i ++) {
			$this->setHorizontal ( $objPHPExcel, $this->arr [$this->typeReport] ['horizontal'] [$i] [0], $this->arr [$this->typeReport] ['horizontal'] [$i] [1], $startRowID + 1, $this->arr [$this->typeReport] ['horizontal'] [$i] [2], $height );
		}
	}
	/**
	 * @author Nguyen Minh Tuan
	 * @name createTable3
	 * @todo create Table for report EngineerResourceUtilization_Detail
	 * @param $objPHPExcel:PHPExcel, $title:array, $data:array
	 */
	private function createTableERUD(PHPExcel $objPHPExcel, $title, $data) {
		$width = count ( $title );
		$height = count ( $data );
		$startColumID = 2;
		$startRowID = 7;
		
		$this->boderHeder ( $objPHPExcel, $this->styleTable, $this->numToStr ( $startColumID ), $startRowID, $width, 1 );
		$countCol = $startColumID;
		for($i = 0; $i < $width; $i ++) {
			$var = $this->numToStr ( $countCol );
			$this->setText1 ( $objPHPExcel, $this->style, $var . $startRowID, $title [$countCol - $startColumID] );
			$countCol ++;
		}
		$this->setAutofilter ( $objPHPExcel, 'C', 7, 7, 1 );
		$countRow = 8;
		$c = - 1;
		$heightdata = count ( $data );
		
		$eID = $data [0] ['eID'];
		
		for($y = 0; $y < $heightdata; $y ++) {
			$countCol = 2;
			$c ++;
			
			for($i = 0; $i < $width; $i ++) {
				$var = $this->numToStr ( $countCol );
				
				$objPHPExcel->setActiveSheetIndex ()->setCellValue ( $var . $countRow, $data [$y] [$title [$countCol - $startColumID]] );
				
				if ($data [$y] ['eID'] != $eID) {
					$this->setMerge ( $objPHPExcel, 'B', $countRow - $c, 1, $c );
					$eID = $data [$y] ['eID'];
					$c = 0;
				}
				
				$countCol ++;
			}
			$countRow ++;
		}
		$this->setMerge ( $objPHPExcel, 'B', $countRow - $c - 1, 1, $c + 1 );
		$this->setVerticalCenter ( $objPHPExcel, 'B', $startRowID + 1, 1, $height );
		for($i = 0; $i < count ( $this->arr [$this->typeReport] ['horizontal'] ); $i ++) {
			$this->setHorizontal ( $objPHPExcel, $this->arr [$this->typeReport] ['horizontal'] [$i] [0], $this->arr [$this->typeReport] ['horizontal'] [$i] [1], $startRowID + 1, $this->arr [$this->typeReport] ['horizontal'] [$i] [2], $height );
		}
		$this->boderHeder ( $objPHPExcel, $this->styleTable, 'B', 8, $width, $height );
		$this->setText ( $objPHPExcel, 'Arial', 10, false, $this->numToStr ( $startColumID ), $startRowID + 1, $width, $height - $emty );
	}
	/**
	 * @author Nguyen Minh Tuan
	 * @name createTable3
	 * @todo create Table for report ProjectResourceUtilization
	 * @param $objPHPExcel:PHPExcel, $title:array, $data:array
	 */
	private function createTablePRU(PHPExcel $objPHPExcel, $title, $data) {
		$width = count ( $title );
		$height = count ( $data );
		$startColumID = 2;
		$startRowID = 7;
		
		$this->boderHeder ( $objPHPExcel, $this->styleTable, $this->numToStr ( $startColumID ), $startRowID, $width, 1 );
		$countCol = $startColumID;
		for($i = 0; $i < $width; $i ++) {
			$var = $this->numToStr ( $countCol );
			$this->setText1 ( $objPHPExcel, $this->style, $var . $startRowID, $title [$countCol - $startColumID] );
			$countCol ++;
		}
		$nextCol = 'H';
		$color = 'E0E0E0';
		$this->boderHeder ( $objPHPExcel, $this->styleTable, $nextCol, 6, 3, 1 );
		$this->cellColor ( $objPHPExcel, $color, $nextCol, 6, 3, 1 );
		$this->cellColor ( $objPHPExcel, $color, 'B', 7, $width, 1 );
		$this->setText1 ( $objPHPExcel, $this->style, $nextCol . '6', 'Next Target' );
		$this->setMerge ( $objPHPExcel, $nextCol, 6, 3, 1 );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B7:I7' )->getAlignment ()->setWrapText ( true );
		$countRow = 8;
		$type = $row [$title [0]];
		$c = 0;
		$emty = 0;
		foreach ( $data as $row ) {
			if ($row [$title [0]] == '' && $row [$title [1]] == '') {
				$emty ++;
			} else {
				$countCol = 3;
				$c ++;
				if ($row [$title [0]] != $type) {
					if ($c > 1)
						$this->setMerge ( $objPHPExcel, 'B', $countRow - $c, 1, $c );
					$type = $row [$title [0]];
					$objPHPExcel->setActiveSheetIndex ()->setCellValue ( 'B' . $countRow, $row [$title [0]] );
					$c = 0;
				}
				for($i = 0; $i < $width; $i ++) {
					$var = $this->numToStr ( $countCol );
					$objPHPExcel->setActiveSheetIndex ()->setCellValue ( $var . $countRow, $row [$title [$countCol - $startColumID]] );
					$countCol ++;
				}
				$countRow ++;
			}
		}
		$this->setMerge ( $objPHPExcel, 'B', $countRow - $c - 1, 1, $c + 1 );
		$this->setVerticalCenter ( $objPHPExcel, 'B', $startRowID + 1, 1, $height );
		for($i = 0; $i < count ( $this->arr [$this->typeReport] ['horizontal'] ); $i ++) {
			$this->setHorizontal ( $objPHPExcel, $this->arr [$this->typeReport] ['horizontal'] [$i] [0], $this->arr [$this->typeReport] ['horizontal'] [$i] [1], $startRowID + 1, $this->arr [$this->typeReport] ['horizontal'] [$i] [2], $height );
		}
		$this->boderHeder ( $objPHPExcel, $this->styleTable, 'B', 8, $width, $height - $emty );
		$this->setText ( $objPHPExcel, 'Arial', 10, false, $this->numToStr ( $startColumID ), $startRowID + 1, $width, $height - $emty );
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name checkNumber
	 * @todo check number is integer
	 * @param $number :
	 *        	number
	 * @return boolean
	 */
	private function checkNumber($number) {
		if (preg_match ( '/^[0-9]+$/', $number ))
			return true;
		else
			return false;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name checkNameCol
	 * @todo checkName is name column excel
	 * @param $nameCol :
	 *        	char
	 * @return boolean
	 */
	private function checkNameCol($nameCol) {
		if (preg_match ( '/^[A-Z]+$/', $nameCol ))
			return true;
		else
			return false;
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name checkPosition
	 * @todo check is name cell
	 * @param $position :
	 *        	string
	 * @return boolean
	 */
	private function checkPosition($position) {
		if (preg_match ( '/^[A-Z]+[1-9][0-9]*$/', $position ))
			return true;
		else
			return false;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setMerge
	 * @todo to Merge cells
	 * @param $objPHPExcel:$object :PHPExcel,
	 *        	$col :string , $row : integer, $width: integer, $height : integer
	 */
	private function setMerge(PHPExcel $object, $col, $row, $width, $height) {
		if (($this->checkNameCol ( $col )) && ($this->checkNumber ( $row )) && ($this->checkNumber ( $width )) && ($this->checkNumber ( $height )))
			$object->getActiveSheet ()->mergeCells ( $this->convert ( $col, $row, $width, $height ) );
		else {
			echo 'Error Value Merge. ';
		}
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setWidth
	 * @todo set Width for column
	 * @param $objPHPExcel:PHPExcel, $col
	 *        	: string , $sizeCol : integer
	 */
	private function setWidth(PHPExcel $obj, $col, $sizeCol) {
		if ($this->checkNameCol ( $col ) && $this->checkNumber ( $sizeCol ))
			$obj->getActiveSheet ()->getColumnDimension ( $col )->setWidth ( $sizeCol );
		else
			echo 'Error Value Set Width. ';
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setHeight
	 * @todo set Height for row
	 * @param $objPHPExcel:PHPExcel, $row
	 *        	: integer, $sizeRow : integer
	 */
	private function setHeight(PHPExcel $obj, $row, $sizeRow) {
		if ($this->checkNumber ( $row ) && $this->checkNumber ( $sizeRow ))
			$obj->getActiveSheet ()->getRowDimension ( $row )->setRowHeight ( $sizeRow );
		else
			echo 'Error Set Height Value';
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setLogo
	 * @todo Set immage for report
	 * @param $objPHPExcel:PHPExcel, $height
	 *        	: number , $width : number , $path: string
	 */
	private function setLogo(PHPExcel $obj, $height, $width, $path) {
		if ($this->checkNumber ( $height ) && $this->checkNumber ( $width )) {
			$objDrawing = new PHPExcel_Worksheet_Drawing ();
			$objDrawing->setPath ( $path );
			$objDrawing->setCoordinates ( 'A1' );
			$objDrawing->setHeight ( $height );
			$objDrawing->setWidth ( $width );
			$objDrawing->setWorksheet ( $obj->getActiveSheet () );
		}
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setText
	 * @todo Set style for text in table : size, bold, center, wrap text
	 * @param $objPHPExcel:PHPExcel, $fontName
	 *        	: string , $size : integer, $bold : boolean, $colName : string, $rowID: integer, $width : integer, $height : integer
	 */
	private function setText(PHPExcel $obj, $fontName, $size, $bold, $colName, $rowID, $width, $height) {
		$obj->getActiveSheet ()->getStyle ( $this->convert ( $colName, $rowID, $width, $height ) )->getFont ()->setBold ( $bold )->setName ( $fontName )->setSize ( $size );
		$obj->getActiveSheet ()->getStyle ( $this->convert ( $colName, $rowID, $width, $height ) )->getAlignment ()->setWrapText ( true );
		$obj->getActiveSheet ()->getStyle ( $this->convert ( $colName, $rowID, $width, $height ) )->getAlignment ()->setVertical ( PHPExcel_Style_Alignment::VERTICAL_CENTER );
	}
	private function setVerticalCenter(PHPExcel $objPHPExcel, $colName, $rowID, $width, $height) {
		$objPHPExcel->getActiveSheet ()->getStyle ( $this->convert ( $colName, $rowID, $width, $height ) )->getAlignment ()->setVertical ( PHPExcel_Style_Alignment::VERTICAL_CENTER );
	}
	private function setHorizontal(PHPExcel $objPHPExcel, $type, $colName, $rowID, $width, $height) {
		$objPHPExcel->getActiveSheet ()->getStyle ( $this->convert ( $colName, $rowID, $width, $height ) )->getAlignment ()->setHorizontal ( $type );
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setText1
	 * @todo Set Text style in array style
	 * @param $objPHPExcel:PHPExcel, $style:
	 *        	array, $position: string, $text: string
	 */
	private function setText1(PHPExcel $obj, array $style, $position, $text) {
		if ($this->checkPosition ( $position )) {
			$obj->getActiveSheet ()->getCell ( $position )->setValue ( $text );
			$obj->getActiveSheet ()->getStyle ( $position )->applyFromArray ( $style );
		} else
			echo 'Eror position for set Text';
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name boderHeder
	 * @todo Set boder for table
	 * @param $objPHPExcel:PHPExcel, $style
	 *        	: array, $col: string , $row :integer, $width: integer, $height: integer
	 */
	private function boderHeder(PHPExcel $objPHPExcel, array $style, $col, $row, $width, $height) {
		$objPHPExcel->getActiveSheet ()->getStyle ( $this->convert ( $col, $row, $width, $height ) )->applyFromArray ( $style );
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name charToNum
	 * @todo convert char[A-Z] to number [1-26]
	 * @param $str: char        	
	 * @return number
	 */
	private function charToNum($str) {
		$arr = array (
				1 => 'A',
				2 => 'B',
				3 => 'C',
				4 => 'D',
				5 => 'E',
				6 => 'F',
				7 => 'G',
				8 => 'H',
				9 => 'I',
				10 => 'J',
				11 => 'K',
				12 => 'L',
				13 => 'M',
				14 => 'N',
				15 => 'O',
				16 => 'P',
				17 => 'Q',
				18 => 'R',
				19 => 'S',
				20 => 'T',
				21 => 'U',
				22 => 'V',
				23 => 'W',
				24 => 'X',
				25 => 'Y',
				26 => 'Z' 
		);
		return array_search ( strtoupper ( $str ), $arr );
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name convert
	 * @todo convert to range in excel (ex : 'A1:E5')
	 * @param $col: string
	 *        	, $row :integer, $width: integer, $height: integer
	 * @return range
	 */
	private function convert($col, $row, $w, $h) {
		$num1 = $this->strToNum ( $col );
		$num2 = $num1 + $w - 1;
		$str1 = $this->numToStr ( $num2 );
		$num3 = $row + $h - 1;
		return $col . $row . ':' . $str1 . $num3;
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name strToNum
	 * @todo convert name column to number
	 * @param $str :
	 *        	string
	 * @return number
	 */
	private function strToNum($str) {
		$leng = strlen ( $str );
		$sum = 0;
		setType ( $sum, 'integer' );
		for($i = 0; $i < $leng; $i ++) {
			$tem1 = $this->charToNum ( $str [$i] );
			$sum = $sum * 26 + $tem1;
		}
		return $sum;
	}
	private function conVertDate($date) {
		$startDate = substr ( $date, 8, 2 ) . '/' . substr ( $date, 5, 2 ) . '/' . substr ( $date, 2, 2 );
		return $startDate;
	}
	
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name numToStr
	 * @todo convert number to colum name in excel
	 * @param $number :
	 *        	integer
	 * @return name colum
	 */
	private function numToStr($number) {
		$arr = array (
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z' 
		);
		$str = '';
		$temp = $number;
		$mod;
		setType ( $mod, 'integer' );
		setType ( $temp, 'integer' );
		while ( ($temp / 26) > 0 ) {
			$mod = $temp % 26;
			$str = $str . $arr [$mod - 1];
			$temp = ( integer ) ($temp / 26);
		}
		return strrev ( $str );
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name cellColor
	 * @todo set Color for range cells
	 * @param $objPHPExcel:PHPExcel, $color:
	 *        	string ,$col: string , $row :integer, $width: integer, $height: integer
	 */
	private function cellColor(PHPExcel $objPHPExcel, $color, $col, $row, $width, $height) {
		$var = $this->convert ( $col, $row, $width, $height );
		$objPHPExcel->getActiveSheet ()->getStyle ( $var )->getFill ()->applyFromArray ( array (
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array (
						'rgb' => $color 
				) 
		) );
	}
	/**
	 *
	 * @author Nguyen Minh Tuan
	 * @name setAutofilter
	 * @todo set filter for colum name
	 * @param $objPHPExcel:PHPExcel, $col:
	 *        	string , $row :integer, $width: integer, $height: integer
	 */
	private function setAutofilter(PHPExcel $object, $col, $row, $width, $height) {
		if ($this->checkNameCol ( $col ) && $this->checkNumber ( $row ) && $this->checkNumber ( $width ) && $this->checkNumber ( $height )) {
			$object->getActiveSheet ()->setAutoFilter ( $this->convert ( $col, $row, $width, $height ) );
		} else
			echo 'Error Autofilter Value';
	}
	// ================END FUNCTION------------------------
}